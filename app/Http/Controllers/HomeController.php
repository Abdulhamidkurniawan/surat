<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Pasien;
use App\Rm;
use App\Surat;
use Artisan;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        return view('home');
    }

    public function dashboard()
    {
        $users = User::latest()->get();
        $valu = $users->count();
        $pasiens = Pasien::latest()->get();
        $valpasien = $pasiens->count();
        $valrms = Surat::latest()->get();
        $valrm = $valrms->count();
        return view('dashboard', compact('valu','valpasien','valrm'));
    }

    public function optimize()
    {
        $exitCode = Artisan::call('optimize');
        return '<h1>Reoptimized class loader</h1>';    }

}
