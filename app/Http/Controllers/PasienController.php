<?php

namespace App\Http\Controllers;

use App\Pasien;
use App\Work;
use Illuminate\Http\Request;
use Excel;
use App\Exports\PasienExport;

class PasienController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pasiens = Pasien::latest()->paginate(3000);
        // foreach ($pasiens as $pasien)
        // {echo $pasien;}
        // dd($pasiens);
        return view('pasien.index', compact('pasiens')); /* kirim var */
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $works = Work::all();
        return view('pasien.create', compact('works')); /* kirim var */
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        function tgl_indo($tanggal){
            $bulan = array (
                1 =>   'Januari',
                'Februari',
                'Maret',
                'April',
                'Mei',
                'Juni',
                'Juli',
                'Agustus',
                'September',
                'Oktober',
                'November',
                'Desember'
            );
            $pecahkan = explode('-', $tanggal);

            // variabel pecahkan 0 = tanggal
            // variabel pecahkan 1 = bulan
            // variabel pecahkan 2 = tahun

            return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
        }
        // dd(tgl_indo($request->ttl));
        Pasien::create([
            'nama' => request('nama'),
            'ktp' => request('ktp'),
            'ttl' => tgl_indo($request->ttl),
            'jk' => request('jk'),
            'pekerjaan' => request('pekerjaan'),
            'alamat' => request('alamat'),
            'hp' => request('hp'),
        ]);
        return redirect()->route('pasien.index')->withSuccess('data berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Pasien  $pasien
     * @return \Illuminate\Http\Response
     */
    public function show(Pasien $pasien)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Pasien  $pasien
     * @return \Illuminate\Http\Response
     */
    public function edit(Pasien $pasien)
    {
        $works = Work::all();
        return view('pasien.edit', compact('pasien','works'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pasien  $pasien
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pasien $pasien)
    {
        // dd($request->all());
        $update = $request->All();
        $pasien->update($update);
        return redirect()->route('pasien.index')->withSuccess('data berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Pasien  $pasien
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pasien $pasien)
    {
        $pasien->delete();

        return redirect()->route('pasien.index')->withDanger('data berhasil dihapus');
    }

    public function excel()
    {
        return Excel::download(new PasienExport, 'pasien.xlsx');
    }
}
