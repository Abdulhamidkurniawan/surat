<?php

namespace App\Http\Controllers;

use App\Pasien;
use App\Rm;
use App\Surat;
use Illuminate\Http\Request;

class RedirectController extends Controller
{
    public function default()
    {
        return view('welcome');
    }

    public function cari()
    {
        return view('cari');
    }

    public function cek(Request $request)
    {
        $ktp = $request->ktp;
        $hasils = Surat::latest()->where('ktp','=', $ktp)->get();
        // dd($hasils);
        if(!$hasils->isEmpty()) {
            // dd($ktp,$hasil);
            return view('hasil', compact('hasils'));
        }
        return view('cari');
    }

    public function hasil()
    {
        return view('hasil');
    }
}
