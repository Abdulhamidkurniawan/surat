<?php

namespace App\Http\Controllers;

use App\Surat;
use App\Tests;
use Illuminate\Http\Request;
use Excel;
use App\Exports\RmExport;
use App\Exports\RmExportDate;
use App\Rm;
use App\Pasien;
use Carbon\Carbon;
use App\Officer;
use App\Bills;
use Auth;

class RmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rms = Surat::select('id','nama','jenis_rm','hasil','tanggal')->where('status','!=','dihapus')->orWhereNull('status')->latest()->get();
        // $rms = json_encode($rms);
        // dd($rms);
        return view('rm.index', compact('rms')); /* kirim var */
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Rm  $rm
     * @return \Illuminate\Http\Response
     */
    public function show(Rm $rm)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Rm  $rm
     * @return \Illuminate\Http\Response
     */
    public function edit(Rm $rm)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Rm  $rm
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Rm $rm)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Rm  $rm
     * @return \Illuminate\Http\Response
     */
    public function destroy(Rm $rm)
    {
    $rm->delete();
    return redirect()->route('rm.index')->withDanger('data berhasil dihapus');
    }

    public function excelbulan(Request $request)
    {
        $tgl = $request->tgl;
        // $rms = Surat::where('tanggal', 'like', '%2020%')->get();
        // return view('rm.excel', compact('rms','tgl'));
        // return (new RmExport)->download('Rm.xlsx');

        // return Excel::download(new RmExport, 'rm.xlsx');

        function tgl_indo($tanggal){
            $bulan = array (
                1 =>   'Januari',
                'Februari',
                'Maret',
                'April',
                'Mei',
                'Juni',
                'Juli',
                'Agustus',
                'September',
                'Oktober',
                'November',
                'Desember'
            );
            $pecahkan = explode('-', $tanggal);

            // variabel pecahkan 0 = tanggal
            // variabel pecahkan 1 = bulan
            // variabel pecahkan 2 = tahun

            return $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
        }
        return (new RmExport)->forYear($tgl)->download('Rekapan '.tgl_indo($tgl).'.xlsx');

        // dd($tgl,date('Y-m'),tgl_indo($tgl));
        $tgl = tgl_indo($tgl);
        // $bln = substr($tgl,3);
        $bln = $tgl;
        $bulan = $request->tgl;
        $rms = Surat::where('tanggal', 'like', '%'.$tgl.'%')->get();
        $ttl = $rms->count();

        // return view('rm.excel', compact('rms','tgl','bln','bulan'));
    }

    public function exceltanggal(Request $request)
    {
        $tgl = $request->tgl;
        // $rms = Surat::where('tanggal', 'like', '%2020%')->get();
        // return view('rm.excel', compact('rms','tgl'));
        // return (new RmExport)->download('Rm.xlsx');
        return (new RmExportDate)->forYear($tgl)->download('Rekapan '.$tgl.'.xlsx');

        // return Excel::download(new RmExport, 'rm.xlsx');
        $bln = substr($tgl,3);
        $rms = Surat::where('tanggal', 'like', '%'.$tgl.'%')->get();
        $ttl = $rms->count();
        // return view('rm.excel', compact('rms','tgl','ttl','bln'));
    }

    public function billindex()
    {
        // $surats = Surat::latest()->get();
        $surats = Surat::select('id','nama','jenis_rm','hasil','tanggal')->latest()->get();
        $tests = Tests::all();
        $array_bln = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
        $pas = Pasien::latest()->limit(1)->get();
        $bills = Bills::latest()->limit(1)->get();
        $htg = Bills::latest()->count();
        foreach ($bills as $bill){}
            // dd($bill->no_urut);
            if ($htg==0){
                $no="00001";
                $tahun =date('Y');}
            else{
                $no=$bill->no_urut+1;
                $tahun= substr($bill->no_surat,-4);}
                // $tahun=2020;
            if (date('Y')!=$tahun){
                $no="00001";
                $tahun =date('Y');}

            // $tahun= substr($bill->no_surat,-4);
            $no_urut=str_pad($no, 5, '0', STR_PAD_LEFT);
            $nomor = (int)$no."/KBB/".$array_bln[date('n')]."/".$tahun;
            // $no_rm = "KBB/".date("dmy")."/".(int)$no;

            $tes = Bills::where('nama','Manual')
                            ->whereDate('created_at',Carbon::today()->toDateString())
                            ->first();
            if($tes!=null){
            $no_urut = $tes->no_urut;
            $nomor = $tes->no_surat;
            $no_rm = $tes->no_rm;
            // dd($tes,$no_urut,$nomor,$no_rm,'ada data');
            }
            // else{dd($tes,$no_urut,$nomor,$no_rm,'kosong');
            // }

            // dd($htg,$no,$no_urut,$nomor);
            $officers = Officer::latest()->get();
            $tests = Tests::latest()->get();
        return view('bill.index', compact('surats','tests','nomor','no_urut','officers')); /* kirim var */
    }

    public function billcreate(Surat $surat)
    {
            $array_bln = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
            $pas = Pasien::latest()->limit(1)->get();
            $bills = Bills::latest()->limit(1)->get();
            $htg = Bills::latest()->count();
            foreach ($bills as $bill){}
            // dd($bill->no_urut);
            if ($htg==0){
                $no="00001";
                $tahun =date('Y');}
            else{
                $no=$bill->no_urut+1;
                $tahun= substr($bill->no_surat,-4);}
                // $tahun=2020;
            if (date('Y')!=$tahun){
                $no="00001";
                $tahun =date('Y');}

                $no_rm = $surat->no_rm;

            // $tahun= substr($bill->no_surat,-4);
            $no_urut=str_pad($no, 5, '0', STR_PAD_LEFT);
            $nomor = (int)$no."/KBB/".$array_bln[date('n')]."/".$tahun;
            // $no_rm = "KBB/".date("dmy")."/".(int)$no;

            $tes = Bills::where('nama',$surat->nama)
                            ->whereDate('created_at',Carbon::today()->toDateString())
                            ->first();
            if($tes!=null){
            $no_urut = $tes->no_urut;
            $nomor = $tes->no_surat;
            $no_rm = $tes->no_rm;
            // dd($tes,$no_urut,$nomor,$no_rm,'ada data');
            }
            // else{dd($tes,$no_urut,$nomor,$no_rm,'kosong');
            // }

            // dd($htg,$no,$no_urut,$nomor);
            $officers = Officer::latest()->get();
            $tests = Tests::latest()->get();
            // dd($surat);
            return view('bill.create', compact('tests','surat','nomor','no_urut','no_rm','officers'));
    }

    public function billcetak(Request $request, Surat $surat)
    {
        function tgl_indo($tanggal){
            $bulan = array (
                1 =>   'Januari',
                'Februari',
                'Maret',
                'April',
                'Mei',
                'Juni',
                'Juli',
                'Agustus',
                'September',
                'Oktober',
                'November',
                'Desember'
            );
            $pecahkan = explode('-', $tanggal);
            return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
        }

        function penyebut($nilai) {
            $nilai = abs($nilai);
            $huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
            $temp = "";
            if ($nilai < 12) {
                $temp = " ". $huruf[$nilai];
            } else if ($nilai <20) {
                $temp = penyebut($nilai - 10). " belas";
            } else if ($nilai < 100) {
                $temp = penyebut($nilai/10)." puluh". penyebut($nilai % 10);
            } else if ($nilai < 200) {
                $temp = " seratus" . penyebut($nilai - 100);
            } else if ($nilai < 1000) {
                $temp = penyebut($nilai/100) . " ratus" . penyebut($nilai % 100);
            } else if ($nilai < 2000) {
                $temp = " seribu" . penyebut($nilai - 1000);
            } else if ($nilai < 1000000) {
                $temp = penyebut($nilai/1000) . " ribu" . penyebut($nilai % 1000);
            } else if ($nilai < 1000000000) {
                $temp = penyebut($nilai/1000000) . " juta" . penyebut($nilai % 1000000);
            } else if ($nilai < 1000000000000) {
                $temp = penyebut($nilai/1000000000) . " milyar" . penyebut(fmod($nilai,1000000000));
            } else if ($nilai < 1000000000000000) {
                $temp = penyebut($nilai/1000000000000) . " trilyun" . penyebut(fmod($nilai,1000000000000));
            }
            return $temp;
        }

        function terbilang($nilai) {
            if($nilai<0) {
                $hasil = "minus ". trim(penyebut($nilai));
            } else {
                $hasil = trim(penyebut($nilai));
            }
            return $hasil;
        }

        $hrgs = Tests::where('nama','=',$request->jenis)->get();
        foreach ($hrgs as $hrg){
            $harga = $hrg->harga;
        }
    $tpl_file = getcwd().'/tpl/bp.rtf';
    $target = getcwd().'/tmp/bp.rtf';
    if (file_exists($tpl_file)) {
    $f = fopen($tpl_file, "r+");
    $isi = fread($f, filesize($tpl_file));
    fclose($f);

    $isi = str_replace('no_surat', $request->no_surat, $isi);
    $isi = str_replace('nama_surat', $surat->nama, $isi);
    $isi = str_replace('tanggal_surat', $request->tgl_surat, $isi);
    $isi = str_replace('rm_surat', $request->no_rm, $isi);
    $isi = str_replace('des_pembayaran', $request->jenis, $isi);
    $isi = str_replace('jenis_rm', $request->jenis, $isi);
    $isi = str_replace('harga', number_format($harga, 0, ".", "."), $isi);
    $isi = str_replace('terbilang', ucwords(terbilang($harga).' Rupiah'), $isi);
    $isi = str_replace('sub_total', number_format($harga, 0, ".", "."), $isi);
    $isi = str_replace('petugas', $request->petugas, $isi);


    $f = fopen($target, "w+");
    fwrite($f, $isi);
    fclose($f);
        $nama_file='Bukti Pembayaran '.preg_replace("/[^a-zA-Z ]/", "",$surat->nama).'.doc';
    // header('Location:'.$target);
    header("Content-disposition: attachment; filename=$nama_file");
    header("Content-type: application/octet-stream");
    readfile($target);

    $tes = Bills::where('nama',$surat->nama)
    ->where('jenis_rm',$request->jenis)
    ->whereDate('created_at',Carbon::today()->toDateString())
    ->first();

    if($tes==null){
    Bills::create([
        'no_surat' => $request->no_surat,
        'no_urut' => $request->no_urut,
        'no_rm' => $request->no_rm,
        'nama' => $surat->nama,
        'jenis_rm' => $request->jenis,
        'jenis_bill' => $request->jenis,
        'sub_total' => $harga,
        'total' => $harga,
        'petugas' => $request->petugas,
        'tanggal' => $request->tgl_surat,
        'tanggal_periksa' => $request->tgl_periksa,
    ]);}

    }else{echo 'File tidak ada!';}
            // return redirect()->route('surat.index')->withSuccess('data berhasil dicetak');
    }
    public function cm(Request $request)
    {
        function tgl_indo($tanggal){
            $bulan = array (
                1 =>   'Januari',
                'Februari',
                'Maret',
                'April',
                'Mei',
                'Juni',
                'Juli',
                'Agustus',
                'September',
                'Oktober',
                'November',
                'Desember'
            );
            $pecahkan = explode('-', $tanggal);
            return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
        }

        function penyebut($nilai) {
            $nilai = abs($nilai);
            $huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
            $temp = "";
            if ($nilai < 12) {
                $temp = " ". $huruf[$nilai];
            } else if ($nilai <20) {
                $temp = penyebut($nilai - 10). " belas";
            } else if ($nilai < 100) {
                $temp = penyebut($nilai/10)." puluh". penyebut($nilai % 10);
            } else if ($nilai < 200) {
                $temp = " seratus" . penyebut($nilai - 100);
            } else if ($nilai < 1000) {
                $temp = penyebut($nilai/100) . " ratus" . penyebut($nilai % 100);
            } else if ($nilai < 2000) {
                $temp = " seribu" . penyebut($nilai - 1000);
            } else if ($nilai < 1000000) {
                $temp = penyebut($nilai/1000) . " ribu" . penyebut($nilai % 1000);
            } else if ($nilai < 1000000000) {
                $temp = penyebut($nilai/1000000) . " juta" . penyebut($nilai % 1000000);
            } else if ($nilai < 1000000000000) {
                $temp = penyebut($nilai/1000000000) . " milyar" . penyebut(fmod($nilai,1000000000));
            } else if ($nilai < 1000000000000000) {
                $temp = penyebut($nilai/1000000000000) . " trilyun" . penyebut(fmod($nilai,1000000000000));
            }
            return $temp;
        }

        function terbilang($nilai) {
            if($nilai<0) {
                $hasil = "minus ". trim(penyebut($nilai));
            } else {
                $hasil = trim(penyebut($nilai));
            }
            return $hasil;
        }
        // dd($request->all());

        if ($request->jenis!=null or $request->jenis!="" or $request->jml!=0){
        $hrgs = Tests::where('nama','=',$request->jenis)->get();
        foreach ($hrgs as $hrg){
            if ($request->jml!="0"){$harga = $hrg->harga;} else {$harga = 0;}
            $sub_total = $request->jml*$hrg->harga;
        }}
        else{$harga = 0;$sub_total=0;}
        if ($request->jenis2!=null or $request->jenis2!="" or $request->jml2!="0"){
            $hrgs = Tests::where('nama','=',$request->jenis2)->get();
        foreach ($hrgs as $hrg){
            if ($request->jml2!="0"){$harga2 = $hrg->harga;} else {$harga2 = 0;}
            $sub_total2 = $request->jml2*$hrg->harga;
        }}
        else{$harga2 = 0;$sub_total2=0;}
        if ($request->jenis3!=null or $request->jenis3!="" or $request->jml3!="0"){
            $hrgs = Tests::where('nama','=',$request->jenis3)->get();
        foreach ($hrgs as $hrg){
            if ($request->jml3!="0"){$harga3 = $hrg->harga;} else {$harga3 = 0;}
            $sub_total3 = $request->jml3*$hrg->harga;
        }}
        else{$harga3 = 0;$sub_total3=0;}
        if ($request->jenis4!=null or $request->jenis4!="" or $request->jml4!="0"){
            $hrgs = Tests::where('nama','=',$request->jenis4)->get();
        foreach ($hrgs as $hrg){
            if ($request->jml4!="0"){$harga4 = $hrg->harga;} else {$harga4 = 0;}
            $sub_total4 = $request->jml4*$hrg->harga;
        }}
        else{$harga4 = 0;$sub_total4=0;}
        if ($request->jenis5!=null or $request->jenis5!="" or $request->jml5!="0"){
            $hrgs = Tests::where('nama','=',$request->jenis5)->get();
        foreach ($hrgs as $hrg){
            if ($request->jml5!="0"){$harga5 = $hrg->harga;} else {$harga5 = 0;}
            $sub_total5 = $request->jml5*$hrg->harga;
        }}
        else{$harga5 = 0;$sub_total5=0;}
    $total = $sub_total+$sub_total2+$sub_total3+$sub_total4+$sub_total5;

    if($harga == 0){$request->jenis=""; $harga=""; $request->jml="";$sub_total="";}else{$harga= number_format($harga, 0, ".", "."); $sub_total= number_format($sub_total, 0, ".", ".");}
    if($harga2 == 0){$request->jenis2=""; $harga2=""; $request->jml2="";$sub_total2="";}else{$harga2= number_format($harga2, 0, ".", "."); $sub_total2= number_format($sub_total2, 0, ".", ".");}
    if($harga3 == 0){$request->jenis3=""; $harga3=""; $request->jml3="";$sub_total3="";}else{$harga3= number_format($harga3, 0, ".", "."); $sub_total3= number_format($sub_total3, 0, ".", ".");}
    if($harga4 == 0){$request->jenis4=""; $harga4=""; $request->jml4="";$sub_total4="";}else{$harga4= number_format($harga4, 0, ".", "."); $sub_total4= number_format($sub_total4, 0, ".", ".");}
    if($harga5 == 0){$request->jenis5=""; $harga5=""; $request->jml5="";$sub_total5="";}else{$harga5= number_format($harga5, 0, ".", "."); $sub_total5= number_format($sub_total5, 0, ".", ".");}

    // dd($request->all(),$total,$harga,$harga2,$harga3,$harga4,$harga5);
    $tpl_file = getcwd().'/tpl/bp_cm.rtf';
    $target = getcwd().'/tmp/bp_cm.rtf';
    if (file_exists($tpl_file)) {
    $f = fopen($tpl_file, "r+");
    $isi = fread($f, filesize($tpl_file));
    fclose($f);

    $isi = str_replace('no_surat', $request->no_surat, $isi);
    $isi = str_replace('nama_surat', $request->nama, $isi);
    $isi = str_replace('tanggal_surat', $request->tgl_surat, $isi);
    $isi = str_replace('rm_surat', $request->no_rm, $isi);
    $isi = str_replace('des_pembayaran', $request->keterangan, $isi);
    $isi = str_replace('jenis_rm1', $request->jenis, $isi);
    $isi = str_replace('jenis_rm2', $request->jenis2, $isi);
    $isi = str_replace('jenis_rm3', $request->jenis3, $isi);
    $isi = str_replace('jenis_rm4', $request->jenis4, $isi);
    $isi = str_replace('jenis_rm5', $request->jenis5, $isi);
    $isi = str_replace('harga1', $harga, $isi);
    $isi = str_replace('harga2', $harga2, $isi);
    $isi = str_replace('harga3', $harga3, $isi);
    $isi = str_replace('harga4', $harga4, $isi);
    $isi = str_replace('harga5', $harga5, $isi);
    $isi = str_replace('jml1', $request->jml, $isi);
    $isi = str_replace('jml2', $request->jml2, $isi);
    $isi = str_replace('jml3', $request->jml3, $isi);
    $isi = str_replace('jml4', $request->jml4, $isi);
    $isi = str_replace('jml5', $request->jml5, $isi);
    $isi = str_replace('sub_total1', $sub_total, $isi);
    $isi = str_replace('sub_total2', $sub_total2, $isi);
    $isi = str_replace('sub_total3', $sub_total3, $isi);
    $isi = str_replace('sub_total4', $sub_total4, $isi);
    $isi = str_replace('sub_total5', $sub_total5, $isi);
    $isi = str_replace('totl', number_format($total, 0, ".", "."), $isi);
    $isi = str_replace('terbilang', ucwords(terbilang($total).' Rupiah'), $isi);
    $isi = str_replace('petugas', $request->petugas, $isi);



    $f = fopen($target, "w+");
    fwrite($f, $isi);
    fclose($f);
        $nama_file='Bukti Pembayaran '.preg_replace("/[^a-zA-Z ]/", "",$request->nama).'.doc';
    // header('Location:'.$target);
    header("Content-disposition: attachment; filename=$nama_file");
    header("Content-type: application/octet-stream");
    readfile($target);

    $tes = Bills::where('nama',$request->nama)
    ->where('jenis_rm',implode(", ", array($request->jenis,$request->jenis2,$request->jenis3,$request->jenis4,$request->jenis5)))
    ->whereDate('created_at',Carbon::today()->toDateString())
    ->first();

    if($tes==null){
    Bills::create([
        'no_surat' => $request->no_surat,
        'no_urut' => $request->no_urut,
        'no_rm' => "Cetak Manual",
        'nama' => $request->nama,
        'jenis_rm' => implode(", ", array($request->jenis,$request->jenis2,$request->jenis3,$request->jenis4,$request->jenis5)),
        'jenis_bill' => implode(", ", array($request->jenis,$request->jenis2,$request->jenis3,$request->jenis4,$request->jenis5)),
        'jumlah' => implode(", ", array($sub_total,$sub_total2,$sub_total3,$sub_total4,$sub_total5)),
        'sub_total' => implode(", ", array($request->jml,$request->jml2,$request->jml3,$request->jml4,$request->jml5)),
        'total' => $total,
        'petugas' => $request->petugas,
        'tanggal' => $request->tgl_surat,
        'tanggal_periksa' => $request->tgl_periksa,
    ]);}
    }else{echo 'File tidak ada!';}
            // return redirect()->route('surat.index')->withSuccess('data berhasil dicetak');
    }

    public function dihapus()
    {
        $rms = Surat::select('id','nama','jenis_rm','hasil','tanggal','status','updated_at','operator','petugas')->where('status','dihapus')->latest()->get();
        // $rms = json_encode($rms);
        // dd($rms);
        return view('rm.dihapus', compact('rms')); /* kirim var */
    }

}
