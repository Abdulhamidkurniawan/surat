<?php

namespace App\Http\Controllers;

use App\Surat;
use App\Rm;
use App\Pasien;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Officer;
use Auth;
use DateTime;

class SuratController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pasiens = Pasien::select('id','nama','ktp','ttl')->latest()->get();

        return view('surat.index', compact('pasiens')); /* kirim var */
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Surat  $surat
     * @return \Illuminate\Http\Response
     */
    public function show(Surat $surat)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Surat  $surat
     * @return \Illuminate\Http\Response
     */
    public function edit(Surat $surat)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Surat  $surat
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Surat $surat)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Surat  $surat
     * @return \Illuminate\Http\Response
     */
    public function destroy(Surat $surat)
    {

        $surat->delete();

        return redirect()->route('rm.index')->withDanger('data berhasil dihapus');
    }

    public function softdel(Surat $surat)
    {
        $surat->update([
            'status' => 'dihapus',
            'operator' => Auth::user()->name,
        ]);

        return redirect()->route('rm.index')->withDanger('data berhasil dihapus');
    }

    public function rapid(Pasien $pasien)
    {
        $array_bln = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
        $pas = Pasien::latest()->limit(1)->get();
        $surats = Surat::latest()->limit(1)->get();
        $htg = Surat::latest()->count();

        foreach ($surats as $surat){}
        // dd($surat->no_urut);
        if ($htg==0){
            $no="00001";
            $tahun =date('Y');}
        else{
            $no=$surat->no_urut+1;
            $tahun= substr($surat->no_surat,-4);}
            // $tahun=2020;
        if (date('Y')!=$tahun){
            $no="00001";
            $tahun =date('Y');}

        // $tahun= substr($surat->no_surat,-4);
        $no_urut=str_pad($no, 5, '0', STR_PAD_LEFT);
        $nomor = (int)$no."/KBB-SK/".$array_bln[date('n')]."/".$tahun;
        $no_rm = "KBB-SK/".date("dmy")."/".(int)$no;

        $tes = Surat::where('nama',$pasien->nama)
                    ->where('jenis_rm','Rapid Test')
                    ->whereDate('created_at',Carbon::today()->toDateString())
                    ->first();
        if($tes!=null){
            $no_urut = $tes->no_urut;
            $nomor = $tes->no_surat;
            $no_rm = $tes->no_rm;
            // dd($tes,$no_urut,$nomor,$no_rm,'ada data');
        }
        // else{dd($tes,$no_urut,$nomor,$no_rm,'kosong');
        // }

        // dd($htg,$no,$no_urut,$nomor);
        $officers = Officer::latest()->get();
        return view('surat.rapid', compact('pasien','nomor','no_urut','no_rm','officers'));
    }

    public function sa(Pasien $pasien)
    {
        $array_bln = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
        $pas = Pasien::latest()->limit(1)->get();
        $surats = Surat::latest()->limit(1)->get();
        $htg = Surat::latest()->count();
        foreach ($surats as $surat){}
        // dd($surat->no_urut);
        if ($htg==0){
            $no="00001";
            $tahun =date('Y');}
        else{
            $no=$surat->no_urut+1;
            $tahun= substr($surat->no_surat,-4);}
            // $tahun=2020;
        if (date('Y')!=$tahun){
            $no="00001";
            $tahun =date('Y');}

        // $tahun= substr($surat->no_surat,-4);
        $no_urut=str_pad($no, 5, '0', STR_PAD_LEFT);
        $nomor = (int)$no."/KBB-SK/".$array_bln[date('n')]."/".$tahun;
        $no_rm = "KBB-SK/".date("dmy")."/".(int)$no;

        $tes = Surat::where('nama',$pasien->nama)
                    ->where('jenis_rm','Swab Test Antigen')
                    ->whereDate('created_at',Carbon::today()->toDateString())
                    ->first();
        if($tes!=null){
            $no_urut = $tes->no_urut;
            $nomor = $tes->no_surat;
            $no_rm = $tes->no_rm;
            // dd($tes,$no_urut,$nomor,$no_rm,'ada data');
        }
        // else{dd($tes,$no_urut,$nomor,$no_rm,'kosong');
        // }

        // dd($htg,$no,$no_urut,$nomor);

        $officers = Officer::latest()->get();
        return view('surat.swab_ag', compact('pasien','nomor','no_urut','no_rm','officers'));    }

    public function sp(Pasien $pasien)
    {
        $array_bln = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
        $pas = Pasien::latest()->limit(1)->get();
        $surats = Surat::latest()->limit(1)->get();
        $htg = Surat::latest()->count();
        foreach ($surats as $surat){}
        // dd($surat->no_urut);
        if ($htg==0){
            $no="00001";
            $tahun =date('Y');}
        else{
            $no=$surat->no_urut+1;
            $tahun= substr($surat->no_surat,-4);}
            // $tahun=2020;
        if (date('Y')!=$tahun){
            $no="00001";
            $tahun =date('Y');}

        // $tahun= substr($surat->no_surat,-4);
        $no_urut=str_pad($no, 5, '0', STR_PAD_LEFT);
        $nomor = (int)$no."/KBB-SK/".$array_bln[date('n')]."/".$tahun;
        $no_rm = "KBB-SK/".date("dmy")."/".(int)$no;

        $tes = Surat::where('nama',$pasien->nama)
                        ->where('jenis_rm','PCR')
                        ->whereDate('created_at',Carbon::today()->toDateString())
                        ->first();
        if($tes!=null){
        $no_urut = $tes->no_urut;
        $nomor = $tes->no_surat;
        $no_rm = $tes->no_rm;
        // dd($tes,$no_urut,$nomor,$no_rm,'ada data');
        }
        // else{dd($tes,$no_urut,$nomor,$no_rm,'kosong');
        // }

        // dd($htg,$no,$no_urut,$nomor);
        $officers = Officer::latest()->get();
        return view('surat.swab_pcr', compact('pasien','nomor','no_urut','no_rm','officers'));
    }

    public function si(Pasien $pasien)
    {
        $array_bln = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
        $pas = Pasien::latest()->limit(1)->get();
        $surats = Surat::latest()->limit(1)->get();
        $htg = Surat::latest()->count();
        foreach ($surats as $surat){}
        // dd($surat->no_urut);
        if ($htg==0){
            $no="00001";
            $tahun =date('Y');}
        else{
            $no=$surat->no_urut+1;
            $tahun= substr($surat->no_surat,-4);}
            // $tahun=2020;
        if (date('Y')!=$tahun){
            $no="00001";
            $tahun =date('Y');}

        // $tahun= substr($surat->no_surat,-4);
        $no_urut=str_pad($no, 5, '0', STR_PAD_LEFT);
        $nomor = (int)$no."/KBB-SK/".$array_bln[date('n')]."/".$tahun;
        $no_rm = "KBB-SK/".date("dmy")."/".(int)$no;

        $tes = Surat::where('nama',$pasien->nama)
                        ->where('jenis_rm','IFA')
                        ->whereDate('created_at',Carbon::today()->toDateString())
                        ->first();
        if($tes!=null){
        $no_urut = $tes->no_urut;
        $nomor = $tes->no_surat;
        $no_rm = $tes->no_rm;
        // dd($tes,$no_urut,$nomor,$no_rm,'ada data');
        }
        // else{dd($tes,$no_urut,$nomor,$no_rm,'kosong');
        // }

        // dd($htg,$no,$no_urut,$nomor);
        $officers = Officer::latest()->get();
        return view('surat.swab_ifa', compact('pasien','nomor','no_urut','no_rm','officers'));    }

        public function sr_pcr(Pasien $pasien)
        {
            function tgl_indo($tanggal){
                $bulan = array (
                    1 =>   'Januari',
                    'Februari',
                    'Maret',
                    'April',
                    'Mei',
                    'Juni',
                    'Juli',
                    'Agustus',
                    'September',
                    'Oktober',
                    'November',
                    'Desember'
                );
                $pecahkan = explode('-', $tanggal);

                // variabel pecahkan 0 = tanggal
                // variabel pecahkan 1 = bulan
                // variabel pecahkan 2 = tahun

                return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
            }

            $tpl_file = getcwd().'/tpl/sr_pcr.rtf';
            $target = getcwd().'/tmp/sr_pcr.rtf';
            if (file_exists($tpl_file)) {
                // $target = getcwd().'/tmp/ak.rtf';
                $f = fopen($tpl_file, "r+");
                $isi = fread($f, filesize($tpl_file));
                fclose($f);
                // dd($tpl_file);
                $pasien->pekerjaan = strtoupper($pasien->pekerjaan);
                $pasien->ttl = strtoupper($pasien->ttl);
                $isi = str_replace('nama_surat', $pasien->nama, $isi);
                $isi = str_replace('ktp_surat', $pasien->ktp, $isi);
                $isi = str_replace('ttl_surat', strtoupper($pasien->ttl), $isi);
                $isi = str_replace('jk_surat', strtoupper($pasien->jk), $isi);
                $isi = str_replace('pekerjaan_surat', strtoupper($pasien->pekerjaan), $isi);
                $isi = str_replace('hp_surat', $pasien->hp, $isi);
                $isi = str_replace('alamat_surat', strtoupper($pasien->alamat), $isi);
                $isi = str_replace('tgl_surat', tgl_indo(date('Y-m-d')), $isi);

                $f = fopen($target, "w+");
                fwrite($f, $isi);
                fclose($f);
                $nama_file='Surat Rujukan PCR '.preg_replace("/[^a-zA-Z ]/", "",$pasien->nama).' '.$pasien->ktp.'.doc';

                    if (file_exists($target)) {
                        header('Content-Description: File Transfer');
                        header('Content-Type: application/octet-stream');
                        header("Content-disposition: attachment; filename=$nama_file");
                        header('Expires: 0');
                        header('Cache-Control: must-revalidate');
                        header('Pragma: public');
                        header('Content-Length: ' . filesize($target));
                        readfile($target);
                        exit;
                        }
                    }
                else{echo 'File tidak ada!';}
            return;
        }

    public function narkoba(Pasien $pasien)
        {
        $array_bln = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
        $pas = Pasien::latest()->limit(1)->get();
        $surats = Surat::latest()->limit(1)->get();
        $htg = Surat::latest()->count();
        foreach ($surats as $surat){}
        // dd($surat->no_urut);
        if ($htg==0){
            $no="00001";
            $tahun =date('Y');}
        else{
            $no=$surat->no_urut+1;
            $tahun= substr($surat->no_surat,-4);}
            // $tahun=2020;
        if (date('Y')!=$tahun){
            $no="00001";
            $tahun =date('Y');}

        // $tahun= substr($surat->no_surat,-4);
        $no_urut=str_pad($no, 5, '0', STR_PAD_LEFT);
        $nomor = (int)$no."/KBB-SK/DOA/".$array_bln[date('n')]."/".$tahun;
        $no_rm = "KBB-SK/DOA/".date("dmy")."/".(int)$no;

        $tes = Surat::where('nama',$pasien->nama)
                        ->where('jenis_rm','DOA')
                        ->whereDate('created_at',Carbon::today()->toDateString())
                        ->first();
        if($tes!=null){
        $no_urut = $tes->no_urut;
        $nomor = $tes->no_surat;
        $no_rm = $tes->no_rm;
        // dd($tes,$no_urut,$nomor,$no_rm,'ada data');
        }
        // else{dd($tes,$no_urut,$nomor,$no_rm,'kosong');
        // }

        // dd($htg,$no,$no_urut,$nomor);
        $officers = Officer::latest()->get();
        return view('surat.narkoba3', compact('pasien','nomor','no_urut','no_rm','officers'));
    }

    public function narkoba5(Pasien $pasien)
        {
        $array_bln = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
        $pas = Pasien::latest()->limit(1)->get();
        $surats = Surat::latest()->limit(1)->get();
        $htg = Surat::latest()->count();
        foreach ($surats as $surat){}
        // dd($surat->no_urut);
        if ($htg==0){
            $no="00001";
            $tahun =date('Y');}
        else{
            $no=$surat->no_urut+1;
            $tahun= substr($surat->no_surat,-4);}
            // $tahun=2020;
        if (date('Y')!=$tahun){
            $no="00001";
            $tahun =date('Y');}

        // $tahun= substr($surat->no_surat,-4);
        $no_urut=str_pad($no, 5, '0', STR_PAD_LEFT);
        $nomor = (int)$no."/KBB-SK/DOA/".$array_bln[date('n')]."/".$tahun;
        $no_rm = "KBB-SK/DOA/".date("dmy")."/".(int)$no;

        $tes = Surat::where('nama',$pasien->nama)
                        ->where('jenis_rm','DOA')
                        ->whereDate('created_at',Carbon::today()->toDateString())
                        ->first();
        if($tes!=null){
        $no_urut = $tes->no_urut;
        $nomor = $tes->no_surat;
        $no_rm = $tes->no_rm;
        // dd($tes,$no_urut,$nomor,$no_rm,'ada data');
        }
        // else{dd($tes,$no_urut,$nomor,$no_rm,'kosong');
        // }

        // dd($htg,$no,$no_urut,$nomor);
        $officers = Officer::latest()->get();
        return view('surat.narkoba5', compact('pasien','nomor','no_urut','no_rm','officers'));
    }

    public function storerapid(Request $request, Pasien $pasien)
    {
        function tgl_indo($tanggal){
            $bulan = array (
                1 =>   'Januari',
                'Februari',
                'Maret',
                'April',
                'Mei',
                'Juni',
                'Juli',
                'Agustus',
                'September',
                'Oktober',
                'November',
                'Desember'
            );
            $pecahkan = explode('-', $tanggal);

            // variabel pecahkan 0 = tanggal
            // variabel pecahkan 1 = bulan
            // variabel pecahkan 2 = tahun

            return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
        }
    if ($request->igg == 'Reaktif' and $request->igm == 'Reaktif'){
        $hsl = 'IgM dan IgG';
        $hasil_surat = 'REAKTIF';
        $tpl_file = getcwd().'/tpl/rapid_pos.rtf';
        $target = getcwd().'/tmp/rapid_pos.rtf';
    }
    else if ($request->igg == 'Reaktif'){
        $hsl = 'IgG';
        $hasil_surat = 'REAKTIF';
        $tpl_file = getcwd().'/tpl/rapid_pos.rtf';
        $target = getcwd().'/tmp/rapid_pos.rtf';
    }
    else if ($request->igm == 'Reaktif'){
        $hsl = 'IgM';
        $hasil_surat = 'REAKTIF';
        $tpl_file = getcwd().'/tpl/rapid_pos.rtf';
        $target = getcwd().'/tmp/rapid_pos.rtf';
    }
    else{$hasil_surat = 'NON REAKTIF';
        $hsl = 'IgM dan IgG';
        $tpl_file = getcwd().'/tpl/rapid_neg.rtf';
        $target = getcwd().'/tmp/rapid_neg.rtf';
    }

    if (file_exists($tpl_file)) {
    // $target = getcwd().'/tmp/ak.rtf';
    $f = fopen($tpl_file, "r+");
    $isi = fread($f, filesize($tpl_file));
    fclose($f);
    // dd($tpl_file);
    $pasien->pekerjaan = strtoupper($pasien->pekerjaan);
    $pasien->ttl = strtoupper($pasien->ttl);
    $isi = str_replace('no_surat', $request->no_surat, $isi);
    $isi = str_replace('nama_surat', $pasien->nama, $isi);
    $isi = str_replace('ktp_surat', $pasien->ktp, $isi);
    $isi = str_replace('ttl_surat', strtoupper($pasien->ttl), $isi);
    $isi = str_replace('jk_surat', strtoupper($pasien->jk), $isi);
    $isi = str_replace('pekerjaan_surat', strtoupper($pasien->pekerjaan), $isi);
    $isi = str_replace('hp_surat', $pasien->hp, $isi);
    $isi = str_replace('alamat_surat', strtoupper($pasien->alamat), $isi);
    $isi = str_replace('hasil_surat', $hasil_surat, $isi);
    $isi = str_replace('hsl', $hsl, $isi);
    $isi = str_replace('tgl_surat', $request->tgl_surat, $isi);
    $isi = str_replace('rm_surat', $request->no_rm, $isi);
    $isi = str_replace('igg_surat', $request->igg, $isi);
    $isi = str_replace('igm_surat', $request->igm, $isi);
    $isi = str_replace('jam_sampel', $request->jam_sampel, $isi);
    $isi = str_replace('jam_periksa', $request->jam_periksa, $isi);
    // dd($request);

    $f = fopen($target, "w+");
    fwrite($f, $isi);
    fclose($f);
        $nama_file='Rapid '.preg_replace("/[^a-zA-Z ]/", "",$pasien->nama).' '.$pasien->ktp.'.doc';
    // // header('Location:'.$target);
    // header("Content-disposition: attachment; filename=$nama_file");
    // header("Content-type: application/octet-stream");
    // readfile($target);

    $tes = Surat::where('nama',$pasien->nama)
    ->where('jenis_rm','Rapid Test')
    ->whereDate('created_at',Carbon::today()->toDateString())
    ->first();

    if($tes==null){
        // dd($request->petugas);

    Surat::create([
        'no_surat' => $request->no_surat,
        'no_urut' => $request->no_urut,
        'no_rm' => $request->no_rm,
        'ktp' => $pasien->ktp,
        'nama' => $pasien->nama,
        'jenis_rm' => 'Rapid Test',
        'hasil' => $hasil_surat,
        'petugas' => $request->petugas,
        'operator' => Auth::user()->name,
        'tanggal' => tgl_indo(date('Y-m-d')),
        'tanggal_surat' => $request->tgl_surat,
        'jam_sampel' => $request->jam_sampel,
        'jam_periksa' => $request->jam_periksa,
    ]);}

    if (file_exists($target)) {
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header("Content-disposition: attachment; filename=$nama_file");
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($target));
        readfile($target);
        exit;}
    }else{echo 'File tidak ada!';}
            // return redirect()->route('surat.index')->withSuccess('data berhasil dicetak');
    }

    public function storesa(Request $request, Pasien $pasien)
    {
        function tgl_indo($tanggal){
            $bulan = array (
                1 =>   'Januari',
                'Februari',
                'Maret',
                'April',
                'Mei',
                'Juni',
                'Juli',
                'Agustus',
                'September',
                'Oktober',
                'November',
                'Desember'
            );
            $pecahkan = explode('-', $tanggal);

            // variabel pecahkan 0 = tanggal
            // variabel pecahkan 1 = bulan
            // variabel pecahkan 2 = tahun

            return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
        }
    // dd($pasien->jenis);
    // if ($pasien->jenis == 'Surat Aktif'){
    //     $tpl_file = getcwd().'/tpl/ak.rtf';
    //     $target = getcwd().'/tmp/ak.rtf';
    // }
    // else if ($pasien->jenis == 'Tugas Akhir'){
    //     $tpl_file = getcwd().'/tpl/spd3.rtf';
    //     $target = getcwd().'/tmp/spd3.rtf';
    // }
    // else if ($pasien->jenis == 'Skripsi'){
    //     $tpl_file = getcwd().'/tpl/spd4.rtf';
    //     $target = getcwd().'/tmp/spd4.rtf';
    // }
    if ($request->hasil == 'Reaktif'){
        $hasil_surat = 'Positif';
        $tpl_file = getcwd().'/tpl/swab_ag_pos.rtf';
        $target = getcwd().'/tmp/swab_ag_pos.rtf';
    }
    else{$hasil_surat = 'Negatif';
        $tpl_file = getcwd().'/tpl/swab_ag_neg.rtf';
        $target = getcwd().'/tmp/swab_ag_neg.rtf';
    }

    if (file_exists($tpl_file)) {
    // $target = getcwd().'/tmp/ak.rtf';
    $f = fopen($tpl_file, "r+");
    $isi = fread($f, filesize($tpl_file));
    fclose($f);
    $pasien->pekerjaan = strtoupper($pasien->pekerjaan);
    $pasien->ttl = strtoupper($pasien->ttl);
    $isi = str_replace('no_surat', $request->no_surat, $isi);
    $isi = str_replace('nama_surat', $pasien->nama, $isi);
    $isi = str_replace('ktp_surat', $pasien->ktp, $isi);
    $isi = str_replace('ttl_surat', strtoupper($pasien->ttl), $isi);
    $isi = str_replace('jk_surat', strtoupper($pasien->jk), $isi);
    $isi = str_replace('pekerjaan_surat', strtoupper($pasien->pekerjaan), $isi);
    $isi = str_replace('hp_surat', $pasien->hp, $isi);
    $isi = str_replace('alamat_surat', strtoupper($pasien->alamat), $isi);
    $isi = str_replace('hasil_surat', $hasil_surat, $isi);
    $isi = str_replace('tgl_surat', $request->tgl_surat, $isi);
    $isi = str_replace('tgl_periksa', $request->tgl_periksa, $isi);
    $isi = str_replace('rm_surat', $request->no_rm, $isi);
    $isi = str_replace('igg_surat', $request->igg, $isi);
    $isi = str_replace('igm_surat', $request->igm, $isi);
    $isi = str_replace('jam_sampel', $request->jam_sampel, $isi);
    $isi = str_replace('jam_periksa', $request->jam_periksa, $isi);

    $f = fopen($target, "w+");
    fwrite($f, $isi);
    fclose($f);
        $nama_file='Swab AG '.preg_replace("/[^a-zA-Z ]/", "",$pasien->nama).' '.$pasien->ktp.'.doc';
    // header('Location:'.$target);
    header("Content-disposition: attachment; filename=$nama_file");
    header("Content-type: application/octet-stream");
    readfile($target);

    $tes = Surat::where('nama',$pasien->nama)
    ->where('jenis_rm','Swab Test Antigen')
    ->whereDate('created_at',Carbon::today()->toDateString())
    ->first();

    if($tes==null){
    Surat::create([
        'no_surat' => $request->no_surat,
        'no_urut' => $request->no_urut,
        'no_rm' => $request->no_rm,
        'ktp' => $pasien->ktp,
        'nama' => $pasien->nama,
        'jenis_rm' => 'Swab Test Antigen',
        'hasil' => $hasil_surat,
        'petugas' => $request->petugas,
        'operator' => Auth::user()->name,
        'tanggal' => $request->tgl_periksa,
        'tanggal_surat' => $request->tgl_surat,
        'jam_sampel' => $request->jam_sampel,
        'jam_periksa' => $request->jam_periksa,
    ]);}

    }else{echo 'File tidak ada!';}
            // return redirect()->route('surat.index')->withSuccess('data berhasil dicetak');
    }

    public function storesi(Request $request, Pasien $pasien)
    {
        function tgl_indo($tanggal){
            $bulan = array (
                1 =>   'Januari',
                'Februari',
                'Maret',
                'April',
                'Mei',
                'Juni',
                'Juli',
                'Agustus',
                'September',
                'Oktober',
                'November',
                'Desember'
            );
            $pecahkan = explode('-', $tanggal);

            // variabel pecahkan 0 = tanggal
            // variabel pecahkan 1 = bulan
            // variabel pecahkan 2 = tahun

            return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
        }
    // dd($pasien->jenis);
    // if ($pasien->jenis == 'Surat Aktif'){
    //     $tpl_file = getcwd().'/tpl/ak.rtf';
    //     $target = getcwd().'/tmp/ak.rtf';
    // }
    // else if ($pasien->jenis == 'Tugas Akhir'){
    //     $tpl_file = getcwd().'/tpl/spd3.rtf';
    //     $target = getcwd().'/tmp/spd3.rtf';
    // }
    // else if ($pasien->jenis == 'Skripsi'){
    //     $tpl_file = getcwd().'/tpl/spd4.rtf';
    //     $target = getcwd().'/tmp/spd4.rtf';
    // }
    if ($request->igg == 'Positif' or $request->igm == 'Positif' ){
        $hasil_surat = 'Positif';
        $tpl_file = getcwd().'/tpl/swab_ifa_pos.rtf';
        $target = getcwd().'/tmp/swab_ifa_pos.rtf';
    }
    else{$hasil_surat = 'Negatif';
        $tpl_file = getcwd().'/tpl/swab_ifa_neg.rtf';
        $target = getcwd().'/tmp/swab_ifa_neg.rtf';
    }

    if (file_exists($tpl_file)) {
    // $target = getcwd().'/tmp/ak.rtf';
    $f = fopen($tpl_file, "r+");
    $isi = fread($f, filesize($tpl_file));
    fclose($f);
    $pasien->pekerjaan = strtoupper($pasien->pekerjaan);
    $pasien->ttl = strtoupper($pasien->ttl);
    $isi = str_replace('no_surat', $request->no_surat, $isi);
    $isi = str_replace('nama_surat', $pasien->nama, $isi);
    $isi = str_replace('ktp_surat', $pasien->ktp, $isi);
    $isi = str_replace('ttl_surat', strtoupper($pasien->ttl), $isi);
    $isi = str_replace('jk_surat', strtoupper($pasien->jk), $isi);
    $isi = str_replace('pekerjaan_surat', strtoupper($pasien->pekerjaan), $isi);
    $isi = str_replace('hp_surat', $pasien->hp, $isi);
    $isi = str_replace('alamat_surat', strtoupper($pasien->alamat), $isi);
    $isi = str_replace('hasil_surat', $hasil_surat, $isi);
    $isi = str_replace('tgl_surat', $request->tgl_surat, $isi);
    $isi = str_replace('tgl_periksa', $request->tgl_periksa, $isi);
    $isi = str_replace('rm_surat', $request->no_rm, $isi);
    $isi = str_replace('igg_surat', $request->igg, $isi);
    $isi = str_replace('igm_surat', $request->igm, $isi);
    $isi = str_replace('s_igg', $request->sigg, $isi);
    $isi = str_replace('s_igm', $request->sigm, $isi);
    $isi = str_replace('jam_sampel', $request->jam_sampel, $isi);
    $isi = str_replace('jam_periksa', $request->jam_periksa, $isi);

    $f = fopen($target, "w+");
    fwrite($f, $isi);
    fclose($f);
        $nama_file='Swab IFA '.preg_replace("/[^a-zA-Z ]/", "",$pasien->nama).' '.$pasien->ktp.'.doc';
    // header('Location:'.$target);
    header("Content-disposition: attachment; filename=$nama_file");
    header("Content-type: application/octet-stream");
    readfile($target);

    $tes = Surat::where('nama',$pasien->nama)
    ->where('jenis_rm','IFA')
    ->whereDate('created_at',Carbon::today()->toDateString())
    ->first();

    if($tes==null){
    Surat::create([
        'no_surat' => $request->no_surat,
        'no_urut' => $request->no_urut,
        'no_rm' => $request->no_rm,
        'ktp' => $pasien->ktp,
        'nama' => $pasien->nama,
        'jenis_rm' => 'IFA',
        'hasil' => $hasil_surat,
        'petugas' => $request->petugas,
        'operator' => Auth::user()->name,
        'tanggal' => $request->tgl_periksa,
        'tanggal_surat' => $request->tgl_surat,
        'jam_sampel' => $request->jam_sampel,
        'jam_periksa' => $request->jam_periksa,
    ]);}

    }else{echo 'File tidak ada!';}
            // return redirect()->route('surat.index')->withSuccess('data berhasil dicetak');
    }

    public function storesp(Request $request, Pasien $pasien)
    {
        $tgl_baru = date('Y-m-d', strtotime($request->tgl_surat));
        $tgl_surat = date('d-m-y', strtotime($request->tgl_surat));
        $tgl_periksa = date('d-m-y', strtotime($request->tgl_periksa));
        function tgl_indo($tanggal){
            $bulan = array (
                1 =>   'Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'
            );
            $pecahkan = explode('-', $tanggal);

            // variabel pecahkan 0 = tanggal
            // variabel pecahkan 1 = bulan
            // variabel pecahkan 2 = tahun

            return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
        }
    // dd($pasien->jenis);
    // if ($pasien->jenis == 'Surat Aktif'){
    //     $tpl_file = getcwd().'/tpl/ak.rtf';
    //     $target = getcwd().'/tmp/ak.rtf';
    // }
    // else if ($pasien->jenis == 'Tugas Akhir'){
    //     $tpl_file = getcwd().'/tpl/spd3.rtf';
    //     $target = getcwd().'/tmp/spd3.rtf';
    // }
    // else if ($pasien->jenis == 'Skripsi'){
    //     $tpl_file = getcwd().'/tpl/spd4.rtf';
    //     $target = getcwd().'/tmp/spd4.rtf';
    // }
    if ($request->ct == 'Positif'){
        $hasil_surat = 'Positif';
        $tpl_file = getcwd().'/tpl/swab_pcr_pos.rtf';
        $target = getcwd().'/tmp/swab_pcr_pos.rtf';
    }
    else{$hasil_surat = 'Negatif';
        $tpl_file = getcwd().'/tpl/swab_pcr_neg.rtf';
        $target = getcwd().'/tmp/swab_pcr_neg.rtf';
    }
    // dd(date('d-m-Y', strtotime($request->tgl_surat)),tgl_indo(date('Y-m-d', strtotime($request->tgl_surat))),date('d-m-Y', strtotime($request->tgl_periksa)),tgl_indo(date('Y-m-d', strtotime($request->tgl_periksa))),tgl_indo($tgl_baru));
    if (file_exists($tpl_file)) {
    // $target = getcwd().'/tmp/ak.rtf';
    $f = fopen($tpl_file, "r+");
    $isi = fread($f, filesize($tpl_file));
    fclose($f);
    $pasien->pekerjaan = strtoupper($pasien->pekerjaan);
    $pasien->ttl = strtoupper($pasien->ttl);
    $isi = str_replace('no_surat', $request->no_surat, $isi);
    $isi = str_replace('nama_surat', $pasien->nama, $isi);
    $isi = str_replace('ktp_surat', $pasien->ktp, $isi);
    $isi = str_replace('ttl_surat', strtoupper($pasien->ttl), $isi);
    $isi = str_replace('jk_surat', strtoupper($pasien->jk), $isi);
    $isi = str_replace('pekerjaan_surat', strtoupper($pasien->pekerjaan), $isi);
    $isi = str_replace('hp_surat', $pasien->hp, $isi);
    $isi = str_replace('alamat_surat', strtoupper($pasien->alamat), $isi);
    $isi = str_replace('hasil_surat', $hasil_surat, $isi);
    $isi = str_replace('tgl_surat', $tgl_surat, $isi);
    $isi = str_replace('tanggal_surat', tgl_indo($tgl_baru), $isi);
    $isi = str_replace('tgl_periksa', $tgl_periksa, $isi);
    $isi = str_replace('rm_surat', $request->no_rm, $isi);
    $isi = str_replace('ct_surat', $request->sct, $isi);
    $isi = str_replace('jam_sampel', $request->jam_sampel, $isi);
    $isi = str_replace('jam_periksa', $request->jam_periksa, $isi);
    $isi = str_replace('jam_hasil', $request->jam_hasil, $isi);

    $f = fopen($target, "w+");
    fwrite($f, $isi);
    fclose($f);
        $nama_file='Swab PCR '.preg_replace("/[^a-zA-Z ]/", "",$pasien->nama).' '.$pasien->ktp.'.doc';
    // header('Location:'.$target);
    // header("Content-disposition: attachment; filename=$nama_file");
    // header("Content-type: application/octet-stream");
    // readfile($target);

    $tes = Surat::where('nama',$pasien->nama)
    ->where('jenis_rm','PCR')
    ->whereDate('created_at',Carbon::today()->toDateString())
    ->first();

    if($tes==null){

    Surat::create([
        'no_surat' => $request->no_surat,
        'no_urut' => $request->no_urut,
        'no_rm' => $request->no_rm,
        'ktp' => $pasien->ktp,
        'nama' => $pasien->nama,
        'jenis_rm' => 'PCR',
        'hasil' => $hasil_surat,
        'petugas' => $request->petugas,
        'operator' => Auth::user()->name,
        'jam_sampel' => $request->jam_sampel,
        'jam_periksa' => $request->jam_periksa,
        'tanggal' => tgl_indo(date('Y-m-d', strtotime($request->tgl_periksa))),
        'tanggal_surat' => tgl_indo(date('Y-m-d', strtotime($request->tgl_surat))),

    ]);}

    if (file_exists($target)) {
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header("Content-disposition: attachment; filename=$nama_file");
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($target));
        readfile($target);
        exit;}

    }else{echo 'File tidak ada!';}
            // return redirect()->route('surat.index')->withSuccess('data berhasil dicetak');
    }

    public function storenarkoba(Request $request, Pasien $pasien)
    {
        $hasil_array=[];
        function tgl_indo($tanggal){
            $bulan = array (
                1 =>   'Januari',
                'Februari',
                'Maret',
                'April',
                'Mei',
                'Juni',
                'Juli',
                'Agustus',
                'September',
                'Oktober',
                'November',
                'Desember'
            );
            $pecahkan = explode('-', $tanggal);

            // variabel pecahkan 0 = tanggal
            // variabel pecahkan 1 = bulan
            // variabel pecahkan 2 = tahun

            return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
        }

    if ($request->mor == 'Positif' or $request->amf == 'Positif' or $request->can == 'Positif' ){
        $hasil_surat = 'Positif';
        $tpl_file = getcwd().'/tpl/narkoba_pos.rtf';
        $target = getcwd().'/tmp/narkoba_pos.rtf';
        if ($request->mor == 'Positif'){array_push($hasil_array,"Morphione");}
        if ($request->amf == 'Positif'){array_push($hasil_array,"Amfethamine");}
        if ($request->can == 'Positif'){array_push($hasil_array,"Canabinoid");}
    }
    else{$hasil_surat = 'Negatif';
        $tpl_file = getcwd().'/tpl/narkoba_neg.rtf';
        $target = getcwd().'/tmp/narkoba_neg.rtf';
    }

    if (file_exists($tpl_file)) {
    // dd(implode(",", $hasil_array));
    // $target = getcwd().'/tmp/ak.rtf';
    $f = fopen($tpl_file, "r+");
    $isi = fread($f, filesize($tpl_file));
    fclose($f);
    $pasien->pekerjaan = strtoupper($pasien->pekerjaan);
    $pasien->ttl = strtoupper($pasien->ttl);
    $isi = str_replace('no_surat', $request->no_surat, $isi);
    $isi = str_replace('nama_surat', $pasien->nama, $isi);
    $isi = str_replace('ktp_surat', $pasien->ktp, $isi);
    $isi = str_replace('ttl_surat', strtoupper($pasien->ttl), $isi);
    $isi = str_replace('jk_surat', strtoupper($pasien->jk), $isi);
    $isi = str_replace('pekerjaan_surat', strtoupper($pasien->pekerjaan), $isi);
    $isi = str_replace('hp_surat', $pasien->hp, $isi);
    $isi = str_replace('alamat_surat', strtoupper($pasien->alamat), $isi);
    $isi = str_replace('hasil_surat', $hasil_surat, $isi);
    $isi = str_replace('tgl_surat', $request->tgl_surat, $isi);
    $isi = str_replace('tgl_periksa', $request->tgl_periksa, $isi);
    $isi = str_replace('rm_surat', $request->no_rm, $isi);
    $isi = str_replace('mor', $request->mor, $isi);
    $isi = str_replace('amf', $request->amf, $isi);
    $isi = str_replace('can', $request->can, $isi);
    $isi = str_replace('jam_sampel', $request->jam_sampel, $isi);
    $isi = str_replace('jam_periksa', $request->jam_periksa, $isi);
    $isi = str_replace('drug_surat', implode(", ", $hasil_array), $isi);
    $isi = str_replace('petugas', $request->petugas, $isi);


    $f = fopen($target, "w+");
    fwrite($f, $isi);
    fclose($f);
        $nama_file='Doa 3P '.preg_replace("/[^a-zA-Z ]/", "",$pasien->nama).' '.$pasien->ktp.'.doc';
    // header('Location:'.$target);
    // header("Content-disposition: attachment; filename=$nama_file");
    // header("Content-type: application/octet-stream");
    // readfile($target);

    $tes = Surat::where('nama',$pasien->nama)
    ->where('jenis_rm','DOA 3P')
    ->whereDate('created_at',Carbon::today()->toDateString())
    ->first();

    if($tes==null){
    Surat::create([
        'no_surat' => $request->no_surat,
        'no_urut' => $request->no_urut,
        'no_rm' => $request->no_rm,
        'ktp' => $pasien->ktp,
        'nama' => $pasien->nama,
        'jenis_rm' => 'DOA 3P',
        'hasil' => $hasil_surat,
        'petugas' => $request->petugas,
        'operator' => Auth::user()->name,
        'tanggal' => $request->tgl_periksa,
        'tanggal_surat' => $request->tgl_surat,
    ]);}

    if (file_exists($target)) {
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header("Content-disposition: attachment; filename=$nama_file");
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($target));
        readfile($target);
        exit;}
    }else{echo 'File tidak ada!';}
            // return redirect()->route('surat.index')->withSuccess('data berhasil dicetak');
    }

    public function storenarkoba5(Request $request, Pasien $pasien)
    {
        $hasil_array=[];
        function tgl_indo($tanggal){
            $bulan = array (
                1 =>   'Januari',
                'Februari',
                'Maret',
                'April',
                'Mei',
                'Juni',
                'Juli',
                'Agustus',
                'September',
                'Oktober',
                'November',
                'Desember'
            );
            $pecahkan = explode('-', $tanggal);

            // variabel pecahkan 0 = tanggal
            // variabel pecahkan 1 = bulan
            // variabel pecahkan 2 = tahun

            return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
        }

    if ($request->mor == 'Positif' or $request->met == 'Positif' or $request->amf == 'Positif' or $request->ben == 'Positif' or $request->can == 'Positif' ){
        $hasil_surat = 'Positif';
        $tpl_file = getcwd().'/tpl/narkoba5_pos.rtf';
        $target = getcwd().'/tmp/narkoba5_pos.rtf';
        if ($request->mor == 'Positif'){array_push($hasil_array,"Morphione");}
        if ($request->met == 'Positif'){array_push($hasil_array,"Methamphetamine");}
        if ($request->amf == 'Positif'){array_push($hasil_array,"Amfethamine");}
        if ($request->ben == 'Positif'){array_push($hasil_array,"Benzodiazepin");}
        if ($request->can == 'Positif'){array_push($hasil_array,"Canabinoid");}
    }
    else{$hasil_surat = 'Negatif';
        $tpl_file = getcwd().'/tpl/narkoba5_neg.rtf';
        $target = getcwd().'/tmp/narkoba5_neg.rtf';
    }

    if (file_exists($tpl_file)) {
    // dd(implode(",", $hasil_array));
    // $target = getcwd().'/tmp/ak.rtf';
    $f = fopen($tpl_file, "r+");
    $isi = fread($f, filesize($tpl_file));
    fclose($f);
    $pasien->pekerjaan = strtoupper($pasien->pekerjaan);
    $pasien->ttl = strtoupper($pasien->ttl);
    $isi = str_replace('no_surat', $request->no_surat, $isi);
    $isi = str_replace('nama_surat', $pasien->nama, $isi);
    $isi = str_replace('ktp_surat', $pasien->ktp, $isi);
    $isi = str_replace('ttl_surat', strtoupper($pasien->ttl), $isi);
    $isi = str_replace('jk_surat', strtoupper($pasien->jk), $isi);
    $isi = str_replace('pekerjaan_surat', strtoupper($pasien->pekerjaan), $isi);
    $isi = str_replace('hp_surat', $pasien->hp, $isi);
    $isi = str_replace('alamat_surat', strtoupper($pasien->alamat), $isi);
    $isi = str_replace('hasil_surat', $hasil_surat, $isi);
    $isi = str_replace('tgl_surat', $request->tgl_surat, $isi);
    $isi = str_replace('tgl_periksa', $request->tgl_periksa, $isi);
    $isi = str_replace('rm_surat', $request->no_rm, $isi);
    $isi = str_replace('mor', $request->mor, $isi);
    $isi = str_replace('met', $request->met, $isi);
    $isi = str_replace('amf', $request->amf, $isi);
    $isi = str_replace('ben', $request->ben, $isi);
    $isi = str_replace('can', $request->can, $isi);
    $isi = str_replace('jam_sampel', $request->jam_sampel, $isi);
    $isi = str_replace('jam_periksa', $request->jam_periksa, $isi);
    $isi = str_replace('drug_surat', implode(", ", $hasil_array), $isi);
    $isi = str_replace('petugas', $request->petugas, $isi);


    $f = fopen($target, "w+");
    fwrite($f, $isi);
    fclose($f);
        $nama_file='Doa 5P '.preg_replace("/[^a-zA-Z ]/", "",$pasien->nama).' '.$pasien->ktp.'.doc';
    // header('Location:'.$target);
    header("Content-disposition: attachment; filename=$nama_file");
    header("Content-type: application/octet-stream");
    readfile($target);

    $tes = Surat::where('nama',$pasien->nama)
    ->where('jenis_rm','DOA 5P')
    ->whereDate('created_at',Carbon::today()->toDateString())
    ->first();

    if($tes==null){
    Surat::create([
        'no_surat' => $request->no_surat,
        'no_urut' => $request->no_urut,
        'no_rm' => $request->no_rm,
        'ktp' => $pasien->ktp,
        'nama' => $pasien->nama,
        'jenis_rm' => 'DOA 5P',
        'hasil' => $hasil_surat,
        'petugas' => $request->petugas,
        'operator' => Auth::user()->name,
        'tanggal' => $request->tgl_periksa,
        'tanggal_surat' => $request->tgl_surat,
    ]);}

    }else{echo 'File tidak ada!';}
            // return redirect()->route('surat.index')->withSuccess('data berhasil dicetak');
    }
}
