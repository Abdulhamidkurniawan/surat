<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pasien extends Model
{
    protected $fillable = ['nama','ktp','ttl','jk','pekerjaan','alamat','hp']; /* yang bsa di isi */
}
