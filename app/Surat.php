<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Surat extends Model
{
    protected $fillable = ['no_urut','no_surat','nama','ktp','ttl','jk','pekerjaan','alamat','jenis_rm','hasil','no_rm','tipe_pasien','dokter','tanggal','tanggal_surat','petugas','operator','jam_sampel','jam_periksa','jam_hasil','status']; /* yang bsa di isi */
}
