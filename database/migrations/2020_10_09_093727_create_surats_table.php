<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuratsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('surats', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('no_urut')->nullable();
            $table->string('no_surat')->nullable();
            $table->string('nama')->nullable();
            $table->string('ktp')->nullable();
            $table->string('ttl')->nullable();
            $table->string('jk')->nullable();
            $table->string('pekerjaan')->nullable();
            $table->string('alamat')->nullable();
            $table->string('jenis_rm')->nullable();
            $table->string('hasil')->nullable();
            $table->string('no_rm')->nullable();
            $table->string('tipe_pasien')->nullable();
            $table->string('dokter')->nullable();
            $table->string('petugas')->nullable();
            $table->string('tanggal')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surats');
    }
}
