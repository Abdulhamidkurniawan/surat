<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReceiptsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('receipts')){
        Schema::create('receipts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('no')->nullable();
            $table->string('nama')->nullable();
            $table->string('terima_dari')->nullable();
            $table->string('layanan')->nullable();
            $table->string('harga')->nullable();
            $table->string('jumlah')->nullable();
            $table->string('total')->nullable();
            $table->string('petugas')->nullable();
            $table->string('tanggal')->nullable();
            $table->timestamps();
        });
    }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('receipts');
    }
}
