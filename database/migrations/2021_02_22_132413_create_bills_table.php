<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bills', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('no_surat')->nullable();
            $table->string('no_urut')->nullable();
            $table->string('no_rm')->nullable();
            $table->string('nama')->nullable();
            $table->string('jenis_bill')->nullable();
            $table->string('jenis_rm')->nullable();
            $table->string('hasil')->nullable();
            $table->string('jumlah')->nullable();
            $table->string('tarif')->nullable();
            $table->string('sub_total')->nullable();
            $table->string('total')->nullable();
            $table->string('petugas')->nullable();
            $table->string('tanggal')->nullable();
            $table->string('tanggal_periksa')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bills');
    }
}
