@extends('layouts.app')

@section('content')
<div class="container">
        <div class="col-md-8 offset-md-2">
                <div class="card">
                        {!! Form::open (['method'=>'GET','url'=>'carialumni','role'=>'search'])!!}
                            <div class="card-header">
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control" name="nim" placeholder="Masukan NIM Mahasiswa" value=""><!-- $borang dari route dan borangController -->
                                    <div class="input-group-append">
                                        <button type="submit" class="btn btn-sm btn-info">Cari Alumni</button>
                                    </div>
                                </div>
                            </div>
                        {!! Form::close()!!}
                        <br>
            <table class="table table-hover" >
                <thead>
                  <tr>
                    <th scope="col"><center>No</center></th>
                    <th scope="col"><center>NIM</center></th>
                    <th scope="col"><center>Nama</center></th>
                    <th scope="col"><center>Jenjang</center></th>
                    <th scope="col"><center>Tahun Angkatan</center></th>
                    <th scope="col"><center>Pilihan</center></th>
                  </tr>
                </thead>
                <?php $c=0;?>
            @foreach ($alumnis as $alumni)
            <tbody>
                <tr>
                  <?php $c=$c+1;?>
                  <th scope="row"><center>{{$c}}</center></th>
                  <td><center>{{$alumni->nim}}</td>
                  <td><a href="{{ route('alumni.show', $alumni) }}" >{{$alumni->nama}}</a></td>
                  <td><center>{{$alumni->jenjang}}</center></td>
                  <td><center>{{$alumni->tahun_angkatan}}</center></td>
                  <td><center><form action="{{ route('alumni.destroy', $alumni) }}" method="post">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }} <!-- membuat delete alumniController bisa dibaca -->
                    <a href="{{ route('alumni.edit', $alumni) }}" class="btn btn-sm btn-primary">Edit</a>
                    <button type="submit" class="btn btn-sm btn-danger">Hapus</button>
            </form></center></td>
                </tr>
              </tbody>
            </table>
            <br>
            @endforeach
            <br>
            {!! $alumnis->render() !!}
        </div>
</div>
@endsection
