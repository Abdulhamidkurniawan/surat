@extends('layouts.app') <!-- --> <!-- include-->

@section('content')
    <div class="container">
        <div class="row">
        <div class="col-md-8 offset-md-2">
                <div class="card">
                        <div class="card-header">{{$alumni->nama}} | <small>{{$alumni->jenjang}}</small></div>
                        <div class="card-body">
                                    <div class="form-group">
                                        <label for="">NIM</label>
                                        <input type="text" class="form-control" name="nim" placeholder="NIM Mahasiswa" value="{{$alumni->nim}}" disabled><!-- $alumni dari route dan alumniController -->
                                    </div>
                                    <div class="form-group">
                                        <label for="">Nama</label>
                                        <input type="text" class="form-control" name="nama" placeholder="Nama Mahasiswa" value="{{$alumni->nama}}" disabled><!-- $alumni dari route dan alumniController -->
                                    </div>
                                    <div class="form-group">
                                        <label for="">Jenis Kelamin</label>
                                        <input type="text" class="form-control" name="jk" placeholder="Nama Mahasiswa" value="{{$alumni->jenis_kelamin}}" disabled><!-- $alumni dari route dan alumniController -->
                                    </div>
                                    <div class="form-group">
                                        <label for="">Hp/Telp</label>
                                        <input type="text" class="form-control" name="hp" placeholder="Telp Mahasiswa" value="{{$alumni->hp}}" disabled><!-- $alumni dari route dan alumniController -->
                                    </div>
                                    <div class="form-group">
                                        <label for="">Jenjang</label>
                                        <input type="text" class="form-control" name="jenjang" placeholder="D-III/D-IV" value="{{$alumni->jenjang}}" disabled><!-- $alumni dari route dan alumniController -->
                                    </div>
                                    <div class="form-group">
                                        <label for="">Tahun Angkatan</label>
                                        <input type="text" class="form-control" name="tahun_angkatan" placeholder="cth: 2019" value="{{$alumni->tahun_angkatan}}" disabled><!-- $alumni dari route dan alumniController -->
                                    </div>
                                    <div class="form-group">
                                        <label for="">Judul</label>
                                        <input type="text" class="form-control" name="judul" placeholder="Judul Tugas Akhir/Skripsi" value="{{$alumni->judul}}" disabled><!-- $alumni dari route dan alumniController -->
                                    </div>
                                    <div class="form-group">
                                        <label for="">Pembimbing 1</label>
                                        <input type="text" class="form-control" name="pembimbing_1" placeholder="Nama Pembimbing 1" value="{{$alumni->pembimbing_1}}" disabled><!-- $alumni dari route dan alumniController -->
                                    </div>
                                    <div class="form-group">
                                        <label for="">Pembimbing-2</label>
                                        <input type="text" class="form-control" name="pembimbing_2" placeholder="Nama Pembimbing 2" value="{{$alumni->pembimbing_2}}" disabled><!-- $alumni dari route dan alumniController -->
                                    </div>
                                    <button type="button" class="btn btn-primary" onclick="window.location='{{ route("alumni.index") }}'">Kembali</button>
                        </div>
                </div>
            <br>
        </div>      
        </div>
    </div>   
@endsection