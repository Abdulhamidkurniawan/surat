@extends('layouts.app')
<script src="{{ asset('bower_components/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.js') }}"></script>
<script>
    $.noConflict();
    jQuery(document).ready(function($) {
        $(function() {
            $("#datepicker").datepicker({
                format: 'dd MM yyyy'
            });
            $("#dp").datepicker({
                format: 'dd MM yyyy'
            });
        });
    });
    // Code that uses other library's $ can follow here.

</script>

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Pembuatan Bukti Pembayaran</div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('bill.cetak', $surat) }}">
                            @csrf

                            <div class="form-group row">
                                <label for="nama" class="col-md-4 col-form-label text-md-right">Nama Lengkap</label>

                                <div class="col-md-6">
                                    <input id="nama" type="text" class="form-control" name="nama"
                                        value="{{ $surat->nama }}" readonly>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="ktp" class="col-md-4 col-form-label text-md-right">Nomor KTP</label>

                                <div class="col-md-6">
                                    <input type="number" id="ktp" type="text" class="form-control" name="ktp"
                                        value="{{ $surat->ktp }}" readonly>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="no_surat" class="col-md-4 col-form-label text-md-right">Nomor Bukti
                                    Pembayaran</label>

                                <div class="col-md-6">
                                    <input id="no_surat" type="text" class="form-control" name="no_surat"
                                        value="{{ $nomor }}" placeholder="No Surat" required autofocus>
                                </div>
                                <input id="no_urut" type="hidden" class="form-control" name="no_urut"
                                    value="{{ $no_urut }}" required>
                            </div>

                            <div class="form-group row">
                                <label for="no_rm" class="col-md-4 col-form-label text-md-right">Nomor RM</label>

                                <div class="col-md-6">
                                    <input id="no_rm" type="text" class="form-control" name="no_rm"
                                        value="{{ $no_rm }}" placeholder="No RM" readonly>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="tgl_periksa" class="col-md-4 col-form-label text-md-right">Tanggal
                                    Pemeriksaan</label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control pull-right" id="datepicker" name="tgl_periksa"
                                        value="{{ $surat->tanggal }}" readonly>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="jenis_rm" class="col-md-4 col-form-label text-md-right">Jenis RM</label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control pull-right" name="jenis_rm"
                                        value="{{ $surat->jenis_rm }}" readonly>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="jenis" class="col-md-4 col-form-label text-md-right">Pemeriksaan</label>

                                <div class="col-md-8">
                                    <select name="jenis" id="jenis" class="form-control select2" required>
                                        <option value="">Pilih Jenis Pemeriksaan</option>
                                        @foreach ($tests as $test)
                                            <option value="{{ $test->nama }}">{{ $test->nama }} - Rp.
                                                {{ $test->harga }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="petugas" class="col-md-4 col-form-label text-md-right">Petugas</label>

                                <div class="col-md-4">
                                    <select name="petugas" id="petugas" class="form-control select2">
                                        @foreach ($officers as $officer)
                                            <option value="{{ $officer->nama }}">{{ $officer->nama }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <button type="button" class="btn btn-success float-right mb-1" data-toggle="modal"
                                    data-target="#modalTambahBarang">Tambah</button>

                            </div>

                            <div class="form-group row">
                                <label for="ttl" class="col-md-4 col-form-label text-md-right">Tanggal Surat</label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control pull-right" id="dp" name="tgl_surat" value=""
                                        required>
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Cetak Surat Keterangan') }}
                                    </button>
                                    <a href="{{ route('surat.index') }}" class="btn btn-danger">Kembali</a>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
