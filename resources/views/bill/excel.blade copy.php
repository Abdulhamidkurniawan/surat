<table border =2>
                <thead>
                <tr>
                    <th colspan="6" valign="center" align="center">Rekapitulas Pemeriksaan Covid 19</th>
                </tr>
                <tr>
                    <th colspan="6" valign="center" align="center">Klinik Bunga Bakung</th>
                </tr>
                <tr>
                    <th colspan="6" valign="center" align="center">Bulan {{$bln}}</th>
                </tr>
                <tr>
                    <th colspan="6" align="center"></th>
                </tr>
                <tr>
                    <th rowspan="2" valign="center" align="center">Tanggal</th>
                    <th rowspan="2" valign="center" align="center">Diperiksa</th>
                    <th colspan="3" valign="center" align="center">Jenis Pemeriksaan</th>
                    <th rowspan="2" valign="center" align="center">Nama</th>
                    <th rowspan="2" valign="center" align="center">No KTP</th>

                <tr>
                    <th width=20 align="center">Rapid Test</th>
                    <th width=20 align="center">Swab Anti Gen</th>
                    <th width=20 align="center">IFA</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($rms as $rm)

                <?php
                if ($rm->jenis_rm == "Rapid Test") {$rt=$rm->hasil;} else {$rt="";}
                if ($rm->jenis_rm == "Swab Test Antigen") {$ag=$rm->hasil;} else {$ag="";}
                if ($rm->jenis_rm == "IFA") {$ifa=$rm->hasil;} else {$ifa="";}
                ?>
                <tr>
                    <td align="center">{{$tgl}}</td>
                    <td align="center"> - </td>
                    <td align="center">{{$rt}}</td>
                    <td align="center">{{$ag}}</td>
                    <td align="center">{{$ifa}}</td>
                    <td>{{$rm->nama}}</td>
                    <td align="center">{{$rm->ktp}}</td>
                </tr>
                @endforeach
                <tr>
                    <td colspan="6" align="center"><b>TOTAL</b></td>
                    <td align="center">{{$ttl}}</td>
                  </tr>
                </tbody>

              </table>
