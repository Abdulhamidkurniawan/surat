<?php 
$rtr=0;
$rtnr=0;
$sagp=0;
$sagn=0;
$sifap=0;
$sifan=0;
$today = \Carbon\Carbon::now();
$hari_pertama = \Carbon\Carbon::parse($today)->startOfMonth()->toDateString();;
$hari_terakhir = \Carbon\Carbon::parse($today)->endOfMonth()->toDateString();;
?>
<table border =2>
                <thead>
                <tr>
                <th colspan="6" valign="center" align="center">Rekapitulas Pemeriksaan Covid 19,{{$hari_pertama}} s.d. {{$hari_terakhir}}</th>
                </tr>
                <tr>
                    <th colspan="6" valign="center" align="center">Klinik Bunga Bakung</th>
                </tr>
                <tr>
                    <th colspan="6" valign="center" align="center">Bulan {{$bln}}</th>
                </tr>
                <tr>
                    <th colspan="6" align="center"></th>
                </tr>
                <tr>
                    <th valign="center" align="center">Tanggal</th>
                    <th valign="center" align="center">Diperiksa</th>
                    <th valign="center" align="center">Jenis Pemeriksaan</th>
                    <th valign="center" align="center">Hasil</th>
                    <th valign="center" align="center">Nama</th>
                    <th valign="center" align="center">No KTP</th>

                </thead>
                <tbody>
                @foreach ($rms as $rm)

                <?php
                if ($rm->jenis_rm == "Rapid Test") {
                    $rt=$rm->hasil;
                    if ($rm->hasil == "REAKTIF") {$rtr=$rtr+1;}
                    else if ($rm->hasil == "NON REAKTIF") {$rtnr=$rtnr+1;}
                } else {$rt="";}
                if ($rm->jenis_rm == "Swab Test Antigen") {
                    $ag=$rm->hasil;
                    if ($rm->hasil == "Positif") {$sagp=$sagp+1;}
                    else if ($rm->hasil == "Negatif") {$sagn=$sagn+1;}
                } else {$ag="";}
                if ($rm->jenis_rm == "IFA") {
                    $ifa=$rm->hasil;
                    if ($rm->hasil == "Positif") {$sifap=$sifap+1;}
                    else if ($rm->hasil == "Negatif") {$sifan=$sifan+1;}
                } else {$ifa="";}
                ?>
                <tr>
                    <td align="center">{{$rm->tanggal}}</td>
                    <td align="center"> - </td>
                    <td align="center">{{$rm->jenis_rm}}</td>
                    <td align="center">{{$rm->hasil}}</td>
                    <td>{{$rm->nama}}</td>
                    <td align="center">{{$rm->ktp}}</td>
                </tr>
                @endforeach
                <tr>
                    <td colspan="5" align="center"><b>TOTAL</b></td>
                    <td align="center">{{$ttl}}</td>
                </tr>
                <tr>
                    <th colspan="6" align="center"></th>
                </tr>
                <tr>
                <th align="center"></th>
                <th align="center"></th>
                <th style="font-weight:bold" align="center">Jenis</th>
                <th style="font-weight:bold" align="center">Jumlah</th>
                <th align="center"></th>
                <th align="center"></th>
                </tr>
                <tr>
                <th align="center"></th>
                <th align="center"></th>
                <th style="font-weight:bold" align="">1. Rapid Test</th>
                <th align="center"></th>
                <th align="center"></th>
                <th align="center"></th>
                </tr>
                <tr>
                <th align="center"></th>
                <th align="center"></th>
                <th align="">     Reaktif</th>
                <th align="center">{{$rtr}}</th>
                <th align="center"></th>
                <th align="center"></th>
                </tr>
                <tr>
                <th align="center"></th>
                <th align="center"></th>
                <th align="">     Non Reaktif</th>
                <th align="center">{{$rtnr}}</th>
                <th align="center"></th>
                <th align="center"></th>
                </tr>
                <tr>
                <th align="center"></th>
                <th align="center"></th>
                <th style="font-weight:bold" align="">2. Swab Anti Gen</th>
                <th align="center"></th>
                <th align="center"></th>
                <th align="center"></th>
                </tr>
                <tr>
                <th align="center"></th>
                <th align="center"></th>
                <th align="">     Positif</th>
                <th align="center">{{$sagp}}</th>
                <th align="center"></th>
                <th align="center"></th>
                </tr>
                <tr>
                <th align="center"></th>
                <th align="center"></th>
                <th align="">     Negatif</th>
                <th align="center">{{$sagn}}</th>
                <th align="center"></th>
                <th align="center"></th>
                </tr>
                <tr>
                <th align="center"></th>
                <th align="center"></th>
                <th style="font-weight:bold" align="">3. IFA</th>
                <th align="center"></th>
                <th align="center"></th>
                <th align="center"></th>
                </tr>
                <tr>
                <th align="center"></th>
                <th align="center"></th>
                <th align="">     Positif</th>
                <th align="center">{{$sifap}}</th>
                <th align="center"></th>
                <th align="center"></th>
                </tr>
                <tr>
                <th align="center"></th>
                <th align="center"></th>
                <th align="">     Negatif</th>
                <th align="center">{{$sifan}}</th>
                <th align="center"></th>
                <th align="center"></th>
                </tr>
                <tr>
                <th align="center"></th>
                <th align="center"></th>
                <th style="font-weight:bold" align="center">Total</th>
                <th style="font-weight:bold" align="center">{{$ttl}}</th>
                <th align="center"></th>
                <th align="center"></th>
                </tr>

                </tbody>

              </table>
