@extends('layouts.dashboard')
<script src="{{ asset('bower_components/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.js') }}"></script>
<script>
    $.noConflict();
    jQuery(document).ready(function($) {
        $(function() {
            $("#datepicker").datepicker({
                format: 'dd MM yyyy'
            });
            $("#dp").datepicker({
                format: 'dd MM yyyy'
            });
        });
    });
    // Code that uses other library's $ can follow here.

</script>
<script>
    function alphaOnly(event) {
        var key = event.keyCode;
        return ((key >= 65 && key <= 90) || key == 8 || key == 32 || key == 37 || key == 38 || key == 39 || key == 40 ||
            key == 46);
    };

</script>
<script>
    function myFunction() {
        var x = document.getElementById("ktest").value;
        //   alert(document.getElementById("kker").value);
    }

</script>
@section('content')
    <?php
    setlocale(LC_ALL, 'IND');
    echo strftime('%d %B %Y');
    ?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Data RM Pasien
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Dashboard</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">

                <div class="box">

                    <!-- /.box-header -->
                    <div class="box-body">
                        <button type="button" class="btn btn-danger float-right mb-1" data-toggle="modal"
                            data-target="#modalTambahBarang">Bukti Pembayaran Manual</button>
                        <p> </p>
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Nama</th>
                                    {{-- <th>No KTP</th> --}}
                                    <th>Jenis Pemeriksaan</th>
                                    <th>Hasil</th>
                                    <th width=100>Tanggal Pemeriksaan</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($surats as $surat)
                                    <tr>
                                        <td>{{ $surat->nama }}</td>
                                        {{-- <td>{{ $surat->ktp }}</td> --}}
                                        <td>{{ $surat->jenis_rm }}</td>
                                        <td>{{ $surat->hasil }}</td>
                                        <td>{{ $surat->tanggal }}</td>
                                        <td>
                                            <center>
                                                <a href="{{ route('bill.create', $surat) }}"
                                                    onclick="return confirm('Cetak Bukti Pembayaran {{ $surat->nama }}?')"
                                                    class="btn btn-sm btn-success">Bukti Pembayaran</a>
                                            </center>
                                        </td>
                                    </tr>
                                @endforeach

                            </tbody>

                        </table>
                        <!-- <div class="form-group row">
                                <label for="tgl_periksa" class="col-md-1 col-form-label text-md-right">Tanggal </label>

                                <div class="col-md-2">
                                    <input type="text" class="form-control pull-right" id="dari" name="tgl_periksa" value="" required>
                                </div>
                                <label for="tgl_periksa" class="col-md-1 col-form-label text-md-right">Hingga</label>

                                <div class="col-md-2">
                                    <input type="text" class="form-control pull-right" id="ke" name="tgl_periksa" value="" required>
                                </div>
                            </div>
                  <a href="{{ route('rm.rmreport') }}" class="btn btn-sm btn-primary">Export Data</a> -->
                        <div class="modal fade" id="modalTambahBarang" tabindex="-1" aria-labelledby="modalTambahBarang"
                            aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Tambah Data Pekerjaan</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <!--FORM TAMBAH BARANG-->
                                        <form method="POST" action="{{ route('bill.cm') }}">
                                            @csrf
                                            <div class="form-group row">
                                                <label for="nama" class="col-md-4 col-form-label text-md-right">Nama
                                                    Instansi</label>

                                                <div class="col-md-6">
                                                    <input id="nama" oninput="let p=this.selectionStart;this.value=this.value.toUpperCase();this.setSelectionRange(p, p);" type="text" class="form-control" name="nama" value="{{ old('nama') }}" required autofocus>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="tnama" class="col-md-4 col-form-label text-md-right">Nomor
                                                    Surat</label>
                                                <div class="col-md-4">
                                                    <input id="no_surat" type="text" class="form-control" name="no_surat"
                                                        value="{{ $nomor }}" placeholder="No Surat" readonly>
                                                </div>
                                                <input id="no_urut" type="hidden" class="form-control" name="no_urut"
                                                    value="{{ $no_urut }}" required>
                                            </div>
                                            <div class="form-group row"><label for="jenis" class="col-md-4 col-form-label text-md-right">Layanan</label>
                                                <div class="col-md-4">
                                                    <select name="jenis" id="jenis" class="form-control select2" required>
                                                        <option value="">Pilih Jenis Pemeriksaan</option>
                                                        @foreach ($tests as $test)
                                                            <option value="{{ $test->nama }}">{{ $test->nama }} - Rp.
                                                                {{ $test->harga }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-md-2">
                                                    <input oninput="let p=this.selectionStart;this.value=this.value.toUpperCase();this.setSelectionRange(p, p);" id="jml" type="number" min="0" class="form-control" name="jml" value="0" autofocus placeholder="jml">
                                                </div>
                                            </div>
                                            <div class="form-group row"><label for="jenis" class="col-md-4 col-form-label text-md-right"> </label>
                                                <div class="col-md-4">
                                                    <select name="jenis2" id="jenis2" class="form-control select2">
                                                        <option value="">Pilih Jenis Pemeriksaan</option>
                                                        @foreach ($tests as $test)
                                                            <option value="{{ $test->nama }}">{{ $test->nama }} - Rp.
                                                                {{ $test->harga }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-md-2">
                                                    <input oninput="let p=this.selectionStart;this.value=this.value.toUpperCase();this.setSelectionRange(p, p);" id="jml2" type="number" min="0" class="form-control" name="jml2" value="0" autofocus placeholder="jml">
                                                </div>
                                            </div>
                                            <div class="form-group row"><label for="jenis" class="col-md-4 col-form-label text-md-right"> </label>
                                                <div class="col-md-4">
                                                    <select name="jenis3" id="jenis3" class="form-control select2">
                                                        <option value="">Pilih Jenis Pemeriksaan</option>
                                                        @foreach ($tests as $test)
                                                            <option value="{{ $test->nama }}">{{ $test->nama }} - Rp.
                                                                {{ $test->harga }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-md-2">
                                                    <input oninput="let p=this.selectionStart;this.value=this.value.toUpperCase();this.setSelectionRange(p, p);" id="jml3" type="number" min="0" class="form-control" name="jml3" value="0" autofocus placeholder="jml">
                                                </div>
                                            </div>
                                            <div class="form-group row"><label for="jenis" class="col-md-4 col-form-label text-md-right"> </label>
                                                <div class="col-md-4">
                                                    <select name="jenis4" id="jenis4" class="form-control select2">
                                                        <option value="">Pilih Jenis Pemeriksaan</option>
                                                        @foreach ($tests as $test)
                                                            <option value="{{ $test->nama }}">{{ $test->nama }} - Rp.
                                                                {{ $test->harga }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-md-2">
                                                    <input oninput="let p=this.selectionStart;this.value=this.value.toUpperCase();this.setSelectionRange(p, p);" id="jml4" type="number" min="0" class="form-control" name="jml4" value="0" autofocus placeholder="jml">
                                                </div>
                                            </div>
                                            <div class="form-group row"><label for="jenis" class="col-md-4 col-form-label text-md-right"> </label>
                                                <div class="col-md-4">
                                                    <select name="jenis5" id="jenis5" class="form-control select2">
                                                        <option value="">Pilih Jenis Pemeriksaan</option>
                                                        @foreach ($tests as $test)
                                                            <option value="{{ $test->nama }}">{{ $test->nama }} - Rp.
                                                                {{ $test->harga }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-md-2">
                                                    <input oninput="let p=this.selectionStart;this.value=this.value.toUpperCase();this.setSelectionRange(p, p);" id="jml5" type="number" min="0" class="form-control" name="jml5" value="0" autofocus placeholder="jml">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="tnama" class="col-md-4 col-form-label text-md-right">Keterangan
                                                    Surat</label>
                                                <div class="col-md-4">
                                                    <textarea id="keterangan" name="keterangan" rows="4" cols="42"></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="petugas"
                                                    class="col-md-4 col-form-label text-md-right">Petugas</label>

                                                <div class="col-md-4">
                                                    <select name="petugas" id="petugas" class="form-control select2">
                                                        @foreach ($officers as $officer)
                                                            <option value="{{ $officer->nama }}">{{ $officer->nama }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="ttl" class="col-md-4 col-form-label text-md-right">Tanggal
                                                    Surat</label>

                                                <div class="col-md-4">
                                                    <input type="text" class="form-control pull-right" id="dp"
                                                        name="tgl_surat" value="" required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="ttest" class="col-md-4 col-form-label text-md-right"></label>
                                                <div class="col-md-4">
                                                    <button type="submit" class="btn btn-primary">Cetak Kwitansi</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="modal-body">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
    </div>
    <!-- right col -->
    <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 0.0.1
        </div>
        <strong>Copyright &copy; 2014-2020 <a href="#">BBDSG</a>
    </footer>
    <div class="control-sidebar-bg"></div>
    </div>
    <!-- ./wrapper -->

    <!-- jQuery 3 -->
    <script src="{{ asset('bower_components/jquery/dist/jquery.min.js') }}"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="{{ asset('bower_components/jquery-ui/jquery-ui.min.js') }}"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
        $.widget.bridge('uibutton', $.ui.button);

    </script>
    <!-- Bootstrap 3.3.7 -->
    <script src="{{ asset('bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <!-- Morris.js charts -->
    <script src="{{ asset('bower_components/raphael/raphael.min.js') }}"></script>
    <script src="{{ asset('bower_components/morris.js/morris.min.js') }}"></script>
    <!-- Sparkline -->
    <script src="{{ asset('bower_components/jquery-sparkline/dist/jquery.sparkline.min.js') }}"></script>
    <!-- jvectormap -->
    <script src="{{ asset('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
    <script src="{{ asset('plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
    <!-- jQuery Knob Chart -->
    <script src="{{ asset('bower_components/jquery-knob/dist/jquery.knob.min.js') }}"></script>
    <!-- daterangepicker -->
    <script src="{{ asset('bower_components/moment/min/moment.min.js') }}"></script>
    <script src="{{ asset('bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <!-- datepicker -->
    <script src="{{ asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.js') }}"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="{{ asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
    <!-- Slimscroll -->
    <script src="{{ asset('bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
    <!-- FastClick -->
    <script src="{{ asset('bower_components/fastclick/lib/fastclick.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('dist/js/adminlte.min.js') }}"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="{{ asset('dist/js/pages/dashboard.js') }}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{ asset('dist/js/demo.js') }}"></script>
    <!-- DataTables -->
    <script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script>
        $(function() {
            $('#example1').dataTable({
                'aaSorting': []
            });
            $('#example2').DataTable({
                'paging': true,
                'lengthChange': false,
                'searching': false,
                'ordering': true,
                'info': true,
                'autoWidth': false
            })
        })

    </script>

@endsection
