@extends('layouts.app') <!-- --> <!-- include-->

@section('content')
    <div class="container">
        <div class="col-md-8 offset-md-2">
                <div class="card">
                        <div class="card-header">Edit Data Borang</div>
                            <div class="card-body">
                                    <form class="" action="{{ route('borang.update', $borang)}}" method="post">
                                            {{ csrf_field() }}
                                            {{ method_field('PATCH') }} <!-- membuat patch borangController bisa dibaca -->
                                            <div class="form-group">
                                                <label for="">Jenjang</label>
                                            <input type="text" class="form-control" name="jenjang" placeholder="Cth : D3/D4" value="{{$borang->jenjang}}"><!-- $borang dari route dan borangController -->
                                            </div>    
                                            <div class="form-group">
                                                    <label for="">Jenis Borang</label>
                                                <input type="text" class="form-control" name="jenis" placeholder="Cth : 3A/3B" value="{{$borang->jenis}}"><!-- $borang dari route dan borangController -->
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Butir Borang</label>
                                                <input type="text" class="form-control" name="butir" placeholder="Cth : 3.1.1" value="{{$borang->butir}}"><!-- $borang dari route dan borangController -->
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Judul Borang</label>
                                                <input type="text" class="form-control" name="judul" placeholder="Judul Borang" value="{{$borang->judul}}"><!-- $borang dari route dan borangController -->
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Link</label>
                                                <input type="text" class="form-control" name="link" placeholder="Link Google Sheets" value="{{$borang->link}}"><!-- $borang dari route dan borangController -->
                                                </div>
                                                <div class="form-group">
                                                    <input type="submit" class="btn btn-primary" value="Save">
                                                </div>
                                            </form>
                            </div>
                    </div> 
        </div>
    </div>   
@endsection