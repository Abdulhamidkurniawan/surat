@extends('layouts.app')

@section('content')
<div class="container">
        <div class="col-md-8 offset-md-2">
                <div class="card">
                    {!! Form::open (['method'=>'GET','url'=>'cariborang','role'=>'search'])!!}
                        <div class="card-header">
                                <div class="input-group mb-3">
                                        <select name="jenjang" class="form-control">
                                                <option value="">Jenjang Borang ...</option>
                                                <option value="D3">D3</option>
                                                <option value="D4">D4</option>
                                        </select>                                         
                                        <div class="input-group-append">
                                            <button type="submit" class="btn btn-sm btn-info">Pilih Jenjang</button>
                                        </div>
                                </div>
                        </div>
                    {!! Form::close()!!}
            @foreach ($borangs as $borang)
                <div class="card-header">                  
                    <a href="{{$borang->link}}" target="_blank">{{$borang->jenis}} - {{$borang->butir}} - {{$borang->judul}}</a> - {{$borang->created_at->diffForHumans()}}
                    <div class="float-right">
                            @if (Auth::user()->jabatan == 'admin')
                            <form action="{{ route('borang.destroy', $borang) }}" method="post">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }} <!-- membuat delete borangController bisa dibaca -->
                                    <a href="{{ route('borang.edit', $borang) }}" class="btn btn-sm btn-primary">Edit</a>
                                    <button type="submit" class="btn btn-sm btn-danger">Hapus</button>
                            </form>
                            @elseif (Auth::user()->jabatan == 'karyawan')
                            @else {{ route('login') }}
                            @endif
                    </div>
                </div>
            @endforeach
            </div>
           
        </div>
</div>
@endsection