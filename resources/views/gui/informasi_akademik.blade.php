@extends('layouts.app_gui')

@section('content')
<div class="container">
        <div class="row">
            <div class="col-9">
                @foreach ($posts as $post)
                <div class="card">
                    <div class="card-header">
                         
                        <a href="{{ route('gui.show', $post) }}" >{{$post->title}}</a> - {{$post->created_at->diffForHumans()}}
                    </div>
                    <div class="card-body">
                        <p><center><img src="{!! url('uploads/file/'.$post->gambar) !!}" style="width: 600px; height: 400px;"></center></p>
                        <p> {!!str_limit($post->content, 300,'...')!!} <a href="{{ route('gui.show', $post) }}" target="blank">[Baca Selengkapnya]</a><hr/></p>
            
                    </div>
                </div>
                <br>
                @endforeach
                <br>
            </div>
            <div class="col-3">
               <div class="card">
                   <br>
                    <p style="text-align: center;"><strong>Info Akademik dan Kemahasiswaan</strong></p> 
                    <div class="card-body">
                    @foreach ($guis as $gui)
                        <a href="{{ route('gui.show', $gui) }}" >{{$gui->title}}</a> - {{$gui->created_at->diffForHumans()}}
                        <br>
                        <br>
                        @endforeach 
                    </div>
               </div>
            </div>
          </div>
</div>
<footer class="fixed-bottom bg-info ">
    <br>
    <p class="text-center align-middle text-muted">© Jurusan Teknik Elektro POLNES 2019</p>
</footer>

@endsection
