@extends('layouts.app_gui')

@section('content')
<div class="container">
        <div class="col-md-auto">
            
            @foreach ($posts as $post)
            <div class="card">
                <div class="card-header">
                     
                    <a>{{$post->title}}</a> - {{$post->created_at->diffForHumans()}}
                </div>
                <div class="card-body">
                    <p> {!!$post->content!!} </p>
        
                </div>
            </div>
            <br>
            @endforeach
            <br>
        </div>
</div>
<footer class="fixed-bottom bg-info ">
    <br>
    <p class="text-center align-middle text-muted">© Jurusan Teknik Elektro POLNES 2019</p>
</footer>

@endsection
