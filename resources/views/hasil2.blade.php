<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('dist/css/AdminLTE.min.css')}}">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{asset('dist/css/skins/_all-skins.min.css')}}">

      <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('bower_components/font-awesome/css/font-awesome.min.css')}}">
      <!-- Ionicons -->
      <link rel="stylesheet" href="{{asset('bower_components/Ionicons/css/ionicons.min.css')}}">
     <!-- daterange picker -->
     <link rel="stylesheet" href="{{asset('bower_components/bootstrap-daterangepicker/daterangepicker.css')}}">
     <!-- bootstrap datepicker -->
     <link rel="stylesheet" href="{{asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background: url({{asset('tpl/bg.jpg')}}) center no-repeat;
                background-size: cover;
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            .center {
            text-align: center;
            }
        </style>
    </head>
    <body oncontextmenu="return false;">
        <div class="flex-center position-ref full-height">
            <div class="col-md-6">

            <div class="card">
                    <div class="card-header"><b>Verifikasi Hasil Lab</b></div>
                    <div class="card-body">
                        @foreach ($hasils as $hasil)

                <div class="form-group row">
                    <label for="nama" class="col-md-4 col-form-label text-md-right">Nama Lengkap</label>

                    <div class="col-md-6">
                        <input id="nama" type="text" class="form-control" name="nama" value="{{$hasil->nama}}" readonly>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="ktp" class="col-md-4 col-form-label text-md-right">Nomor KTP</label>

                    <div class="col-md-6">
                        <input type="number" id="ktp" type="text" class="form-control" name="ktp" value="{{$hasil->ktp}}" readonly>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="hp" class="col-md-4 col-form-label text-md-right">Nomor Surat</label>

                    <div class="col-md-6">
                        <input type="text" id="ns" type="text" class="form-control" name="ns" value="{{$hasil->no_surat}}" readonly>
                    </div>
                </div>

                                    <div class="form-group row mb-0">
                                        <div class="col-md-6 offset-md-4">
                                            <button type="submit" class="btn btn-primary">
                                                {{ __('Cari Lagi') }}
                                            </button>
                                        </div>
                                    </div>
                    @endforeach
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
