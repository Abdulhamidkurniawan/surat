<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Portal Teknik Elektro</title>

    <!-- Bootstrap -->
    <link rel="shortcut icon" href="{{ asset('img/favicon.ico') }}">
  <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
	<link href="{{asset('css/font-awesome.min.css')}}" rel="stylesheet">
  <link href="{{asset('font-awesome/4.6.3/css/font-awesome.min.css')}}" rel="stylesheet">
	<link href="{{asset('css/animate.css')}}" rel="stylesheet">
	<link href="{{asset('css/prettyPhoto.css')}}" rel="stylesheet">
	<link href="{{asset('css/stylep.css')}}" rel="stylesheet" />
  </head>
  <body>
	<header>
		<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
			<div class="navigation">
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse.collapse">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<div class="navbar-brand">
              <a href="/"><img src="img/logo-polnes.png" alt="Teknik Elektro-Polnes Official Web"  height="70"></a>
						</div>
					</div>

					<div class="navbar-collapse collapse">
						<div class="menu">
							<ul class="nav nav-tabs" role="tablist">

								<li role="presentation"><a href="contact.html">E-Portal</a></li>
							</ul>
						</div>
					</div>
			  </div>
			</div>
		</nav>
	</header>

	<!--/#main-slider-->

	<div class="feature">
		<div class="container">
			<div class="text-center">
				<div class="col-md-3">
					<div class="hi-icon-wrap hi-icon-effect wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="100ms" >
            <a href="{{asset('wordpress/')}}"><i class="fa fa-laptop"></i></a>
						<h2>Profil</h2>
					</div>
				</div>
				<div class="col-md-3">
					<div class="hi-icon-wrap hi-icon-effect wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="200ms" >
						<a href="{{ route('gui.informasi_akademik') }}"><i class="fa fa-sitemap"></i>
						<h2>Informasi Akademik</h2>
					</div>
				</div>
				<!-- <div class="col-md-3">
					<div class="hi-icon-wrap hi-icon-effect wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms" >
						<a href="{{ route('gui.ealumni') }}"><i class="fa fa-trophy"></i>
						<h2>E-Alumni</h2>
					</div>
				</div> -->
				<!-- <div class="col-md-3">
					<div class="hi-icon-wrap hi-icon-effect wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="400ms" >
						<a href="profil.php"><i class="fa fa-bookmark-o"></i>
						<h2>Berita</h2>
						<p>Quisque eu ante at tortor imperdiet gravida nec sed turpis phasellus.</p>
					</div>
				</div> -->
            <div class="col-md-3">
          <div class="hi-icon-wrap hi-icon-effect wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms" >
            <a href="{{ 'http://siak.polnes.ac.id' }}"><i class="fa fa-credit-card"></i>
            <h2>SIM Polnes</h2>
          </div>
            </div>
        <div class="col-md-3">
          <div class="hi-icon-wrap hi-icon-effect wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="400ms" >
            <a href="{{asset('ff/')}}"><i class="fa fa-folder"></i>
            <h2>E-Modul</h2>
          </div>
        </div>
        <div class="col-md-3">
          <div class="hi-icon-wrap hi-icon-effect wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="500ms" >
            <a href="{{ route('gui.kalender_akademik') }}"><i class="fa fa-calendar-o"></i>
            <h2>Kalender Akademik</h2>
          </div>
        </div>
        <!-- <div class="col-md-3">
          <div class="hi-icon-wrap hi-icon-effect wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="700ms" >
            <a href="{{asset('elearning/')}}"><i class="fa fa-book"></i>
            <h2>E-Learning</h2>
          </div>
        </div> -->
        <div class="col-md-3">
          <div class="hi-icon-wrap hi-icon-effect wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="700ms" >
            <a href="{{ 'http://e-journal.polnes.ac.id/index.php/poligrid' }}"><i class="fa fa-cloud"></i>
            <h2>Journal Poligrid</h2>
          </div>
        </div>
        <div class="col-md-3">
            <div class="hi-icon-wrap hi-icon-effect wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="800ms" >
                <a href="https://docs.google.com/forms/d/e/1FAIpQLSc2dIDO2ncyrv4PxYsyzjki08pkD4YmI0uSl5VhslXbznfUvw/viewform"><i class="fa fa-book"></i>
                <h2>Tugas Akhir/SKRIPSI</h2>
            </div>
        </div>
        <div class="col-md-3">
          <div class="hi-icon-wrap hi-icon-effect wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="900ms" >
            <a href="{{asset('sionlap/')}}"><i class="fa fa-wrench"></i>
            <h2>SIONLAP</h2>
          </div>
        </div>
        <div class="col-md-3">
            <div class="hi-icon-wrap hi-icon-effect wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="1000ms" >
              <a href="{{ route('gui.lokasi') }}"><i class="fa fa-map-marker"></i>
              <h2>Lokasi</h2>
            </div>
          </div>
        <div class="col-md-3">
          <div class="hi-icon-wrap hi-icon-effect wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="1100ms" >
            <a href="{{ route('login') }}"><i class="fa fa-users"></i>
            <h2>User</h2>
          </div>
        </div>
<!--
        <div class="col-md-3">
          <div class="hi-icon-wrap hi-icon-effect wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="900ms" >
            <a href="profil.php"><i class="fa fa-cloud"></i>
            <h2>Friendly Code</h2>
          </div>
        </div>
        <div class="col-md-3">
          <div class="hi-icon-wrap hi-icon-effect wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="1000ms" >
            <a href="profil.php"><i class="fa fa-cloud"></i>
            <h2>Friendly Code</h2>
          </div>
        </div>
        <div class="col-md-3">
          <div class="hi-icon-wrap hi-icon-effect wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="1100ms" >
            <a href="profil.php"><i class="fa fa-cloud"></i>
            <h2>Friendly Code</h2>
          </div> -->
        </div>
			</div>
		</div>
	</div>

  <footer>
    <div class="footer">
      <div class="container">
        <div class="social-icon">
          <div class="col-md-4">
            <ul class="social-network">
              <li><a href="#" class="fb tool-tip" title="Facebook"><i class="fa fa-facebook"></i></a></li>
              <li><a href="#" class="twitter tool-tip" title="Twitter"><i class="fa fa-twitter"></i></a></li>
              <li><a href="#" class="gplus tool-tip" title="Google Plus"><i class="fa fa-google-plus"></i></a></li>
              <li><a href="#" class="linkedin tool-tip" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
              <li><a href="#" class="ytube tool-tip" title="You Tube"><i class="fa fa-youtube-play"></i></a></li>
            </ul>
          </div>
        </div>

        <div class="col-md-4 col-md-offset-4">
          <div class="copyright"><a target="_blank" href="index.php" title="Free Twitter Bootstrap WordPress Themes and HTML templates">Teknik Elektro</a>. All Rights Reserved.
          </div>
                    <!--
                        All links in the footer should remain intact.
                        Licenseing information is available at: http://bootstraptaste.com/license/
                        You can buy this theme without footer links online at: http://bootstraptaste.com/buy/?theme=Company
                    -->
        </div>
      </div>
      <div class="pull-right">
        <a href="#home" class="scrollup"><i class="fa fa-angle-up fa-3x"></i></a>
      </div>
    </div>
  </footer>

   <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="js/jquery-2.1.1.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
  <script src="js/wow.min.js"></script>
  <script src="js/functions.js"></script>

  </body>
  </html>
