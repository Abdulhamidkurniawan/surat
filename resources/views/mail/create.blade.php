@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
                <div class="card-body">
                    <form method="POST" action="{{ route('mail.store') }}">
                        @csrf
                        {{-- {{ route('register') }} --}}
                        <div class="form-group">
                                                    <label for="">Nomor Surat</label>
                                                <input type="text" class="form-control" name="nomor" placeholder="Nomor Surat" value="{{ old('nomor') }}" required><!-- $mail dari route dan mailController -->
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Tujuan</label>
                                                <input type="text" class="form-control" name="tujuan" placeholder="Tujuan Surat" value="{{ old('tujuan') }}" required><!-- $mail dari route dan mailController -->
                                                </div>
                                                <div class="form-group">
                                                    <label for="">NIM</label>
                                                <input type="text" class="form-control" name="nim" placeholder="NIM Mahasiswa" value="{{ old('nim') }}" required><!-- $mail dari route dan mailController -->
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Nama</label>
                                                <input type="text" class="form-control" name="nama" placeholder="Nama Mahasiswa" value="{{ old('nama') }}" required><!-- $mail dari route dan mailController -->
                                                </div>
                                                <div class="form-group">
                                                    <label for="">E-Mail</label>
                                                    <input type="text" class="form-control" name="email" placeholder="E-mail Mahasiswa" value="{{ old('email') }}" required><!-- $mail dari route dan mailController -->
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Hp/Telp</label>
                                                <input type="text" class="form-control" name="hp" placeholder="Telp Mahasiswa" value="{{ old('hp') }}" required><!-- $mail dari route dan mailController -->
                                                </div>
                                                <div class="form-group">
                                                    <input type="submit" class="btn btn-primary" value="Save">
                                                </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
