@extends('layouts.app') <!-- --> <!-- include-->

@section('content')
    <div class="container">
        <div class="col-md-8 offset-md-2">
                <div class="card">
                        <div class="card-header">Edit Surat</div>
                            <div class="card-body">
                                    <form class="" action="{{ route('mail.update', $mail)}}" method="post">
                                            {{ csrf_field() }}
                                            {{ method_field('PATCH') }} <!-- membuat patch mailController bisa dibaca -->
                                                <div class="form-group">
                                                    <label for="">Nomor Surat</label>
                                                <input type="text" class="form-control" name="nomor" placeholder="Nomor Surat" value="{{$mail->nomor}}"><!-- $mail dari route dan mailController -->
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Tujuan</label>
                                                <input type="text" class="form-control" name="tujuan" placeholder="Tujuan Surat" value="{{$mail->tujuan}}"><!-- $mail dari route dan mailController -->
                                                </div>
                                                <div class="form-group">
                                                    <label for="">NIM</label>
                                                <input type="text" class="form-control" name="nim" placeholder="NIM Mahasiswa" value="{{$mail->nim}}"><!-- $mail dari route dan mailController -->
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Nama</label>
                                                <input type="text" class="form-control" name="nama" placeholder="Nama Mahasiswa" value="{{$mail->nama}}"><!-- $mail dari route dan mailController -->
                                                </div>
                                                <div class="form-group">
                                                    <label for="">E-Mail</label>
                                                    <input type="text" class="form-control" name="email" placeholder="E-mail Mahasiswa" value="{{$mail->email}}"><!-- $mail dari route dan mailController -->
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Hp/Telp</label>
                                                <input type="text" class="form-control" name="hp" placeholder="Telp Mahasiswa" value="{{$mail->hp}}"><!-- $mail dari route dan mailController -->
                                                </div>
                                                <div class="form-group">
                                                    <input type="submit" class="btn btn-primary" value="Save">
                                                </div>
                                            </form>
                            </div>
                    </div> 
        </div>
    </div>   
@endsection