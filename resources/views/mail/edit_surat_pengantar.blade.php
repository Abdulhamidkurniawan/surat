@extends('layouts.app') <!-- --> <!-- include-->

@section('content')
    <div class="container">
        <div class="col-md-8 offset-md-2">
                <div class="card">
                        <div class="card-header">Edit mail Pengantar</div>
                            <div class="card-body">
                                    <form class="" action="{{ route('mail.update', $mail)}}" method="post">
                                            {{ csrf_field() }}
                                            {{ method_field('PATCH') }} <!-- membuat patch mailController bisa dibaca -->
                                            <div class="form-group">
                                                <label for="">NIM</label>
                                            <input type="text" class="form-control" name="nim" placeholder="NIM Mahasiswa" value="{{$mail->nim}}" required><!-- $mail dari route dan mailController -->
                                            </div>
                                            <div class="form-group">
                                                <label for="">Nama</label>
                                            <input type="text" class="form-control" name="nama" placeholder="Nama Mahasiswa" value="{{$mail->nama}}" required><!-- $mail dari route dan mailController -->
                                            </div>
                                            <div class="form-group">
                                                <label for="">E-Mail</label>
                                                <input type="text" class="form-control" name="email" placeholder="E-mail Mahasiswa" value="{{$mail->email}}" required><!-- $mail dari route dan mailController -->
                                            </div>
                                            <div class="form-group">
                                                <label for="">Hp/Telp</label>
                                            <input type="text" class="form-control" name="hp" placeholder="Telp Mahasiswa" value="{{$mail->hp}}" required><!-- $mail dari route dan mailController -->
                                            </div>
                                            <div class="form-group">
                                                <input type="hidden"  class="form-control" name="jenis" value="mail Aktif" required><!-- $barang dari route dan barangController -->
                                            </div>
                                            <div class="form-group">
                                                <label for="">Program Studi</label>
                                                <select name="prodi" class="form-control">
                                                    @foreach ($jurusans as $jurusan)   <!-- $categories dari PostController -->
                                                    <option value="{{$jurusan->jurusan}}"
                                                    @if ($jurusan->jurusan === $mail->prodi)  
                                                        selected
                                                    @endif
                                                    > {{$jurusan->jurusan}} </option>
                                                    @endforeach 
                                                 </select>                                                
                                            </div>
                                            <div class="form-group">
                                                <input type="submit" class="btn btn-primary" value="Save">
                                            </div>
                                            </form>
                            </div>
                    </div> 
        </div>
    </div>   
@endsection