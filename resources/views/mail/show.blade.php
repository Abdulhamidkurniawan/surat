@extends('layouts.app') <!-- --> <!-- include-->

@section('content')
    <div class="container">
        <div class="row">
        <div class="col-md-8 offset-md-2">
                <div class="card">
                        <div class="card-header">{{$post->title}} | <small>{{$post->category->name}}</small></div>
                        <div class="card-body">
                            <p> {{$post->content}} </p>
                        </div>
                </div>
            <br>
        </div>      
        </div>
    </div>   
@endsection