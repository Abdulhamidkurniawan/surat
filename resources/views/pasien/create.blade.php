@extends('layouts.app')
<script src="{{asset('bower_components/jquery/dist/jquery.min.js')}}"></script>
<script src="{{asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.js')}}"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<!-- <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> -->
<script>
$.noConflict();
jQuery( document ).ready(function( $ ) {
  $( function() {
  $( "#datepicker" ).datepicker({
    format: 'yyyy-mm-dd',
    changeMonth: true,
    changeYear: true});
} );
});
// Code that uses other library's $ can follow here.
</script>
<!-- <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"></script>
<script src="{{asset('js/jquery-dropdown-datepicker.min.js')}}"></script> -->
<script>
$.noConflict();
jQuery( document ).ready(function( $ ) {
$("#date").dropdownDatepicker({
    defaultDateFormat: 'yyyy-mm-dd',
    displayFormat: 'dmy',
    minYear: 1940,
    allowFuture:false,
    submitFormat: 'yyyy-mm-dd'
});
});
</script>

<script>
function alphaOnly(event) {
  var key = event.keyCode;
  return ((key >= 65 && key <= 90) || key == 8 || key == 32|| key == 37|| key == 38|| key == 39|| key == 40|| key == 46);
};
</script>
<script>
    function myFunction() {
      var x = document.getElementById("kker").value;
    //   alert(document.getElementById("kker").value);
    }
    </script>
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Data Pasien</div>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
                <div class="card-body">
                    <form method="POST" action="{{ route('pasien.store') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="nama" class="col-md-4 col-form-label text-md-right">Nama Lengkap</label>
                            {{-- onkeydown="return alphaOnly(event);" --}}
                            <div class="col-md-6">
                                <input id="nama" oninput="let p=this.selectionStart;this.value=this.value.toUpperCase();this.setSelectionRange(p, p);" type="text" class="form-control" name="nama" value="{{ old('nama') }}" required autofocus>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="ktp" class="col-md-4 col-form-label text-md-right">Nomor KTP</label>

                            <div class="col-md-6">
                                <input type="number" id="ktp" type="text" class="form-control" name="ktp" value="{{ old('ktp') }}" required autofocus>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="ttl" class="col-md-4 col-form-label text-md-right">Tanggal Lahir</label>

                            <div class="col-md-6">
                                <input autocomplete="off" id="datepicker" type="text" class="form-control" name="ttl" value="{{ old('ttl') }}" required autofocus>
                            </div>
                        </div>

                        <div class="form-group row">
                                <label for="jk"class="col-md-4 col-form-label text-md-right">Jenis Kelamin</label>
                                <div class="col-md-6">
                                <select name="jk" id="jk" class="form-control select2">
                                    <option value="LAKI-LAKI">LAKI-LAKI</option>
                                    <option value="PEREMPUAN">PEREMPUAN</option>
                                </select>
                                @error('jk')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                </div>
                            </div>

                        <div class="form-group row">
                            <label for="pekerjaan" class="col-md-4 col-form-label text-md-right">Pekerjaan</label>

                            <div class="col-md-4">
                            <select name="pekerjaan" id="pekerjaan" class="form-control select2">
                            @foreach ($works as $work)
                                    <option value="{{$work->pekerjaan}}">{{$work->pekerjaan}}</option>
                            @endforeach
                                </select>
                                @error('pekerjaan')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror                            </div>
                            <button type="button" class="btn btn-success float-right mb-1" data-toggle="modal" data-target="#modalTambahBarang">Tambah</button>

                        </div>

                        <div class="form-group row">
                            <label for="alamat" class="col-md-4 col-form-label text-md-right">Alamat</label>

                            <div class="col-md-6">
                            <textarea name="alamat" id="alamat" oninput="let p=this.selectionStart;this.value=this.value.toUpperCase();this.setSelectionRange(p, p);" class="form-control" placeholder="Alamat">{{ old('alamat') }}</textarea>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="hp" class="col-md-4 col-form-label text-md-right">Nomor HP</label>

                            <div class="col-md-6">
                                <input type="number" id="hp" type="text" class="form-control" name="hp" value="{{ old('hp') }}" placeholder="08xx" required autofocus>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Tambah Data') }}
                                </button>
                                <a href="{{ route('pasien.index') }}" class="btn btn-danger">Kembali</a>
                            </div>
                        </div>
                    </form>
                    <div class="modal fade" id="modalTambahBarang" tabindex="-1" aria-labelledby="modalTambahBarang" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<h5 class="modal-title">Tambah Data Pekerjaan</h5>
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>
</div>
<div class="modal-body">
<!--FORM TAMBAH BARANG-->
<form method="POST" action="{{ route('pekerjaan.store') }}">
@csrf
<div class="form-group row">
<label for="tker" class="col-md-4 col-form-label text-md-right">Daftar Tindakan</label>
<div class="col-md-4">
<input oninput="let p=this.selectionStart;this.value=this.value.toUpperCase();this.setSelectionRange(p, p);" id="tker" type="text" class="form-control" name="tker" value="{{ old('tker') }}" autofocus>
</div>
<button type="submit" class="btn btn-primary">Tambah</button>
</form>
</div>
<div class="modal-body">
<!--FORM TAMBAH BARANG-->
<form method="POST" action="{{ route('pekerjaan.destroy', $work) }}">
    {{ csrf_field() }}
    {{ method_field('DELETE') }}
<div class="form-group row">
<label for="tker" class="col-md-4 col-form-label text-md-right"></label>
<div class="col-md-4">
    <select name="kker" id="kker" class="form-control select2">
        @foreach ($works as $work)
                <option value="{{$work->id}}">{{$work->pekerjaan}}</option>
        @endforeach
            </select></div>
<button type="submit" class="btn btn-danger">Hapus</button>
{{-- <button type="button" onclick="myFunction()">Try it</button> --}}
</form>
</div>
</div>

<!--END FORM TAMBAH BARANG-->
</div>
</div>
</div>
</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
