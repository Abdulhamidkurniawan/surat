@extends('layouts.app') <!-- --> <!-- include-->

@section('content')
    <div class="container">
        <div class="col-md-8 offset-md-2">
                <div class="card">
                        <div class="card-header">Edit Data Pasien</div>
                            <div class="card-body">
                            <form class="" action="{{ route('pasien.update', $pasien)}}" method="post">
                                            {{ csrf_field() }}
                                            {{ method_field('PATCH') }} <!-- membuat patch PostController bisa dibaca -->

                        <div class="form-group row">
                            <label for="nama" class="col-md-4 col-form-label text-md-right">Nama Lengkap</label>

                            <div class="col-md-6">
                                <input id="nama" type="text" class="form-control" name="nama" value="{{$pasien->nama}}" required autofocus>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="ktp" class="col-md-4 col-form-label text-md-right">Nomor KTP</label>

                            <div class="col-md-6">
                                <input type="number" id="ktp" type="text" class="form-control" name="ktp" value="{{$pasien->ktp}}" required autofocus>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="ttl" class="col-md-4 col-form-label text-md-right">Tempat, Tanggal Lahir</label>

                            <div class="col-md-6">
                                <input id="ttl" type="text" class="form-control" name="ttl" value="{{$pasien->ttl}}" required autofocus>
                            </div>
                        </div>

                        <div class="form-group row">
                                <label for="jk"class="col-md-4 col-form-label text-md-right">Jenis Kelamin</label>
                                <div class="col-md-6">
                                <select name="jk" id="jk" class="form-control select2">
                                    <option value="LAKI-LAKI" @if ($pasien->jk === "LAKI-LAKI") selected @endif >LAKI-LAKI</option>
                                    <option value="PEREMPUAN" @if ($pasien->jk === "PEREMPUAN") selected @endif >PEREMPUAN</option>
                                </select>
                                @error('jk')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                </div>
                            </div>

                        <div class="form-group row">
                            <label for="pekerjaan" class="col-md-4 col-form-label text-md-right">Pekerjaan</label>

                            <div class="col-md-6">
                                <select name="pekerjaan" class="form-control">
                                    @foreach ($works as $work)   <!-- $categories dari PostController -->
                                    <option value="{{$work->pekerjaan}}"
                                    @if ($work->pekerjaan === $pasien->pekerjaan) selected
                                    @endif > {{$work->pekerjaan}} </option>
                                            @endforeach
                                   </select>

                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="alamat" class="col-md-4 col-form-label text-md-right">Alamat</label>

                            <div class="col-md-6">
                            <textarea name="alamat" id="alamat"  class="form-control" placeholder="Alamat">{{$pasien->alamat}}</textarea>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="hp" class="col-md-4 col-form-label text-md-right">Nomor HP</label>

                            <div class="col-md-6">
                                <input type="number" id="hp" type="text" class="form-control" name="hp" value="{{$pasien->hp}}" placeholder="08xx" required autofocus>
                            </div>
                        </div>

                                            <div class="form-group row mb-0">
                                                <div class="col-md-6 offset-md-4">
                                                    <button type="submit" class="btn btn-primary">
                                                        {{ __('Simpan') }}
                                                    </button>
                                                </div>
                                            </div>
                                            </form>
                            </div>
                    </div>
        </div>
    </div>
@endsection
