@extends('layouts.app') <!-- --> <!-- include-->

@section('content')
    <div class="container">
        <div class="col-md-12 offset-md-0">
                <div class="card">
                        <div class="card-header">Edit Post</div>
                            <div class="card-body">
                                    <form class="" action="{{ route('post.update', $post)}}" enctype="multipart/form-data" method="post">
                                            {{ csrf_field() }}
                                            {{ method_field('PATCH') }} <!-- membuat patch PostController bisa dibaca -->
                                                <div class="form-group">
                                                    <label for="">Title</label>
                                                <input type="text" class="form-control" name="title" placeholder="Post Title" value="{{$post->title}}"><!-- $post dari route dan PostController -->
                                                </div>
                                                <div class="form-group">
                                                        <label for="">Kategori</label>
                                                        <select name="category_id" id="" class="form-control">
                                                                @foreach ($categories as $category)   <!-- $categories dari PostController -->
                                                        <option 
                                                        value="{{$category->id}}"
                                                        @if ($category->id === $post->category_id)  
                                                            selected
                                                        @endif
                                                        > {{$category->name}} </option>
                                                                @endforeach     
                                                        </select>
                                                </div>
                                                <div class="form-group">
                                                    <img src="{{ url('uploads/file/'.$post->gambar) }}" style="width: 150px; height: 150px;">                            
                                                </div>
                                                <div class="form-group">
                                                    <input type="file" class="form-control" name="gambar" placeholder="Upload Gambar" value="" style="width:250px">
                                                </div>
                                                <div class="form-group">
                                                        <label for="">Content</label>
                                                <textarea name="content" class="form-control" rows="5" placeholder="Post Content">{!!$post->content!!}</textarea>
                                                </div>
                                                <div class="form-group">
                                                    <input type="submit" class="btn btn-primary" value="Save">
                                                </div>
                                            </form>
                                            <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
        <script>
         var route_prefix = "{{ url(config('lfm.url_prefix', config('lfm.prefix'))) }}";
        </script>      

          <!-- TinyMCE init -->
  <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
  <script>
    var editor_config = {
      path_absolute : "",
      selector: "textarea[name=content]",
      plugins: [
        "link image"
      ],
      relative_urls: false,
      height: 129,
      file_browser_callback : function(field_name, url, type, win) {
        var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
        var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

        var cmsURL = editor_config.path_absolute + route_prefix + '?field_name=' + field_name;
        if (type == 'image') {
          cmsURL = cmsURL + "&type=Images";
        } else {
          cmsURL = cmsURL + "&type=Files";
        }

        tinyMCE.activeEditor.windowManager.open({
          file : cmsURL,
          title : 'Filemanager',
          width : x * 0.8,
          height : y * 0.8,
          resizable : "yes",
          close_previous : "no"
        });
      }
    };

    tinymce.init(editor_config);
  </script>
                            </div>
                    </div> 
        </div>
    </div>   
@endsection