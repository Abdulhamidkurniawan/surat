@extends('layouts.app')

@section('content')
<div class="container">
        <div class="col-md-8 offset-md-2">
            
            @foreach ($posts as $post)
            <div class="card">
                <div class="card-header">
                     
                    <a href="{{ route('post.show', $post) }}" >{{$post->title}} - </a>{{$post->created_at->diffForHumans()}}

                    <div class="float-right">

                        <form action="{{ route('post.destroy', $post) }}" method="post">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }} <!-- membuat delete PostController bisa dibaca -->
                                <a href="{{ route('post.edit', $post) }}" class="btn btn-sm btn-primary">Edit</a>
                                <button type="submit" class="btn btn-sm btn-danger">Hapus</button>
                        </form>
                    </div>
                </div>
                <div class="card-body">
                    <p> {{str_limit($post->content, 100,'...')}} </p>
                </div>
            </div>
            <br>
            @endforeach
            <br>
            {!! $posts->render() !!}
        </div>
</div>
@endsection