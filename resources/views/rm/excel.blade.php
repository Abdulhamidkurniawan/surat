<?php
use Illuminate\Support\Facades\DB;
$rtr=0;
$rtnr=0;
$sagp=0;
$sagn=0;
$sifap=0;
$sifan=0;
// $today = \Carbon\Carbon::now();
$hari_pertama = \Carbon\Carbon::parse($bulan)->startOfMonth()->toDateString();
$hari_terakhir = \Carbon\Carbon::parse($bulan)->endOfMonth()->toDateString();
// $hari_terakhir = \Carbon\Carbon::parse($bulan)->endOfMonth()->add(1, 'day')->toDateString();
$period = \Carbon\CarbonPeriod::create($hari_pertama, $hari_terakhir);
$st=0;
$fn=0;
// $posts= App\Posts::allPosts(postcat)
$ttrtr=0;
$ttrtnr=0;
$ttsagp=0;
$ttsagn=0;
$ttsifap=0;
$ttsifan=0;
$ttspcrp=0;
$ttspcrn=0;
$ttl=0;
?>
<table border =2>
                <thead>
                <tr>
                <th colspan="11" valign="center" align="center">Rekapitulas Pemeriksaan Covid 19</th>
                </tr>
                <tr>
                    <th colspan="11" valign="center" align="center">Klinik Bunga Bakung</th>
                </tr>
                <tr>
                    <th colspan="11" valign="center" align="center">Bulan {{$tgl}}</th>
                </tr>
                <tr>
                    <th colspan="11" align="center"></th>
                </tr>
                <tr>
                    <th rowspan="3" valign="center" align="center">Tanggal</th>
                    <!-- <th rowspan="3" valign="center" align="center">Diperiksa</th> -->
                    <th colspan="8" valign="center" align="center">Jenis Pemeriksaan</th>
                    <th rowspan="3" valign="center" align="center">Total</th>

                <tr>
                    <th colspan="2" width=20 align="center">Rapid Test</th>
                    <th colspan="2" width=20 align="center">Swab Anti Gen</th>
                    <th colspan="2" width=20 align="center">IFA</th>
                    <th colspan="2" width=20 align="center">PCR</th>
                </tr>
                <tr>
                <th width=20 align="center">Reaktif</th>
                <th width=20 align="center">Non Reaktif</th>
                <th width=20 align="center">Positif</th>
                <th width=20 align="center">Negatif</th>
                <th width=20 align="center">Positif</th>
                <th width=20 align="center">Negatif</th>
                <th width=20 align="center">Positif</th>
                <th width=20 align="center">Negatif</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($period as $date)
                <tr>
                <td align="center">{{$date->format('Y-m-d')}}</td>

                <?php
                $rtr=0;
                $rtnr=0;
                $sagp=0;
                $sagn=0;
                $sifap=0;
                $sifan=0;
                $spcrp=0;
                $spcrn=0;
                $tth=0;
                $excels= DB::table('surats')
                        ->whereDate('created_at', 'like', '%'.$date->format('Y-m-d').'%')
                        ->get();
                ?>
                @foreach ($excels as $excel)
                <?php

                if ($excel->jenis_rm == "Rapid Test") {
                    $rt=$excel->hasil;
                    if ($excel->hasil == "REAKTIF") {$rtr=$rtr+1;}
                    else if ($excel->hasil == "NON REAKTIF") {$rtnr=$rtnr+1;}
                } else {$rt="";}
                if ($excel->jenis_rm == "Swab Test Antigen") {
                    $ag=$excel->hasil;
                    if ($excel->hasil == "Positif") {$sagp=$sagp+1;}
                    else if ($excel->hasil == "Negatif") {$sagn=$sagn+1;}
                } else {$ag="";}
                if ($excel->jenis_rm == "IFA") {
                    $ifa=$excel->hasil;
                    if ($excel->hasil == "Positif") {$sifap=$sifap+1;}
                    else if ($excel->hasil == "Negatif") {$sifan=$sifan+1;}
                } else {$ifa="";}
                if ($excel->jenis_rm == "PCR") {
                    $pcr=$excel->hasil;
                    if ($excel->hasil == "Positif") {$spcrp=$spcrp+1;}
                    else if ($excel->hasil == "Negatif") {$spcrn=$spcrn+1;}
                } else {$pcr="";}
                ?>
                @endforeach
                    <td align="center">{{$rtr}}</td>
                    <td align="center">{{$rtnr}}</td>
                    <td align="center">{{$sagp}}</td>
                    <td align="center">{{$sagn}}</td>
                    <td align="center">{{$sifap}}</td>
                    <td align="center">{{$sifan}}</td>
                    <td align="center">{{$spcrp}}</td>
                    <td align="center">{{$spcrn}}</td>
                    <td align="center">{{$tth=$rtr+$rtnr+$sagp+$sagn+$sifap+$sifan+$spcrp+$spcrn}}</td>
                </tr>
                <?php
                $ttrtr=$ttrtr+$rtr;
                $ttrtnr=$ttrtnr+$rtnr;
                $ttsagp=$ttsagp+$sagp;
                $ttsagn=$ttsagn+$sagn;
                $ttsifap=$ttsifap+$sifap;
                $ttsifan=$ttsifan+$sifan;
                $ttspcrp=$ttspcrp+$spcrp;
                $ttspcrn=$ttspcrn+$spcrn;
                $ttl=$ttl+$tth;
                ?>
                @endforeach
                <tr>
                    <td align="center"><b>TOTAL</b></td>
                    <td align="center"><b>{{$ttrtr}}</b></td>
                    <td align="center"><b>{{$ttrtnr}}</b></td>
                    <td align="center"><b>{{$ttsagp}}</b></td>
                    <td align="center"><b>{{$ttsagn}}</b></td>
                    <td align="center"><b>{{$ttsifap}}</b></td>
                    <td align="center"><b>{{$ttsifan}}</b></td>
                    <td align="center"><b>{{$ttspcrp}}</b></td>
                    <td align="center"><b>{{$ttspcrn}}</b></td>
                    <td align="center"><b>{{$ttl}}</b></td>
                </tr>
                </tbody>
</table>

<table border =2>
    <thead>
        <tr>
            <th colspan="9" align="center"></th>
        </tr>
        <tr>
            <th colspan="9" align="center"></th>
        </tr>
        <tr>
            <th colspan="9" align="center"></th>
        </tr>
    <tr>
        <th valign="center" align="center">Tanggal</th>
    <?php
    $officers= DB::table('officers')
            ->where('nama','!=','')
            ->get();
    ?>
        @foreach ($officers as $officer)
        <th valign="center" align="center">{{ $officer->nama }}</th>
        @endforeach
    {{-- <th valign="center" align="center">Total</th> --}}
    </tr>
    </thead>
    <tbody>
        @foreach ($period as $date)
        <?php $tp=0; ?>
        <tr>
            <td align="center">{{$date->format('Y-m-d')}}</td>
            @foreach ($officers as $officer)
            <?php
            $hitung= DB::table('surats')->whereDate('created_at', 'like', '%'.$date->format('Y-m-d').'%')
            ->where('petugas', 'like', '%'.$officer->nama.'%')->count();
            ?>
            <td valign="center" align="center">{{ $hitung }}</td>
            @endforeach
            {{-- <td valign="center" align="center">{{ $tp }}</td> --}}
        </tr>
        @endforeach
        <tr>
            <td align="center">TOTAL</td>
            @foreach ($officers as $officer)
            <?php
            $hitung= DB::table('surats')->whereMonth('created_at', substr($bulan,5))
            ->where('petugas', 'like', '%'.$officer->nama.'%')->count();
            ?>
            <td valign="center" align="center">{{ $hitung }}</td>
            <?php $tp=$tp+$hitung; ?>
            @endforeach
            {{-- <td valign="center" align="center">{{ $tp }}</td> --}}
        </tr>
    </tbody>
</table>


