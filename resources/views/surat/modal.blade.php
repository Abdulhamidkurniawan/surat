<div class="modal fade" id="modalTambahBarang" tabindex="-1" aria-labelledby="modalTambahBarang" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Tambah Data Pekerjaan</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                            </div>
                            <div class="modal-body">
                                <!--FORM TAMBAH BARANG-->
                                <form method="POST" action="{{ route('officer.store') }}">
                                {{ csrf_field() }}
                                <div class="form-group row">
                                    <label for="tker" class="col-md-4 col-form-label text-md-right">Petugas</label>
                                    <div class="col-md-4">
                                        <input oninput="let p=this.selectionStart;this.value=this.value.toUpperCase();this.setSelectionRange(p, p);" id="tof" type="text" class="form-control" name="tof" value="{{ old('tof') }}" autofocus>
                                        <input id="nopas" type="hidden" class="form-control" name="nopas" value="{{$pasien->id}}" required>
                                    </div>
                                    <button type="submit" class="btn btn-primary">Tambah</button>
                                </form>
                                </div>
                                <form method="POST" action="{{ route('officer.destroy', $officer) }}">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                <div class="form-group row">
                                    <label for="tker" class="col-md-4 col-form-label text-md-right"> </label>
                                    <div class="col-md-4">
                                    <select name="kof" id="kof" class="form-control select2">
                                        @foreach ($officers as $officer)
                                            <option value="{{$officer->id}}">{{$officer->nama}}</option>
                                        @endforeach
                                    </select>   
                                    <input id="nopas" type="hidden" class="form-control" name="nopas" value="{{$pasien->id}}" required>                                    
                                </div>
                                    <button type="submit" class="btn btn-danger">Hapus</button>
                                </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>