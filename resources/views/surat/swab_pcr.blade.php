@extends('layouts.app')
<script src="{{asset('bower_components/jquery/dist/jquery.min.js')}}"></script>
<script src="{{asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.js')}}"></script>
<script>
$.noConflict();
jQuery( document ).ready(function( $ ) {
  $( function() {
  $( "#datepicker" ).datepicker({
    format: 'dd-mm-yyyy'});
  $( "#dp" ).datepicker({
    format: 'dd-mm-yyyy'});
    } );
});
// Code that uses other library's $ can follow here.
</script>

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Pembuatan Surat Keterangan</div>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
                <div class="card-body">
                <form method="POST" action="{{ route('surat.storesp', $pasien)}}">
                        @csrf

                        <div class="form-group row">
                            <label for="nama" class="col-md-4 col-form-label text-md-right">Nama Lengkap</label>

                            <div class="col-md-6">
                                <input id="nama" type="text" class="form-control" name="nama" value="{{$pasien->nama}}" disabled>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="ktp" class="col-md-4 col-form-label text-md-right">Nomor KTP</label>

                            <div class="col-md-6">
                                <input type="number" id="ktp" type="text" class="form-control" name="ktp" value="{{$pasien->ktp}}" disabled>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="ttl" class="col-md-4 col-form-label text-md-right">Tempat, Tanggal Lahir</label>

                            <div class="col-md-6">
                                <input id="ttl" type="text" class="form-control" name="ttl" value="{{$pasien->ttl}}" disabled>
                            </div>
                        </div>

                        <div class="form-group row">
                                <label for="jk"class="col-md-4 col-form-label text-md-right">Jenis Kelamin</label>
                                <div class="col-md-6">
                                <input id="jk" type="text" class="form-control" name="jk" value="{{$pasien->jk}}" disabled>

                                @error('jk')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                </div>
                            </div>

                        <div class="form-group row">
                            <label for="pekerjaan" class="col-md-4 col-form-label text-md-right">Pekerjaan</label>

                            <div class="col-md-6">
                                <input id="pekerjaan" type="text" class="form-control" name="pekerjaan" value="{{$pasien->pekerjaan}}" disabled>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="alamat" class="col-md-4 col-form-label text-md-right">Alamat</label>

                            <div class="col-md-6">
                            <textarea name="alamat" id="alamat"  class="form-control" placeholder="Alamat" disabled>{{$pasien->alamat}}</textarea>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="hp" class="col-md-4 col-form-label text-md-right">Nomor HP</label>

                            <div class="col-md-6">
                                <input type="number" id="hp" type="text" class="form-control" name="hp" value="{{$pasien->hp}}" placeholder="08xx" disabled>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="no_surat" class="col-md-4 col-form-label text-md-right">Nomor Surat</label>

                            <div class="col-md-6">
                                <input id="no_surat" type="text" class="form-control" name="no_surat" value="{{$nomor}}" placeholder="No Surat" required autofocus>
                            </div>
                                <input id="no_urut" type="hidden" class="form-control" name="no_urut" value="{{$no_urut}}" required>
                        </div>

                        <div class="form-group row">
                            <label for="no_rm" class="col-md-4 col-form-label text-md-right">Nomor RM</label>

                            <div class="col-md-6">
                            <input id="no_rm" type="text" class="form-control" name="no_rm" value="{{$no_rm}}" placeholder="No RM" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="tgl_periksa" class="col-md-4 col-form-label text-md-right">Tanggal Pemeriksaan</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control pull-right" id="datepicker" name="tgl_periksa" value="" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="jam_sampel" class="col-md-4 col-form-label text-md-right">Jam Sampel</label>

                            <div class="col-md-3">
                                <input type="time" class="form-control pull-right" name="jam_sampel" placeholder="Tekan Simbol Jam" value="" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="jam_periksa" class="col-md-4 col-form-label text-md-right">Jam Periksa</label>

                            <div class="col-md-3">
                                <input type="time" class="form-control pull-right" name="jam_periksa" placeholder="Tekan Simbol Jam" value="" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="jam_hasil" class="col-md-4 col-form-label text-md-right">Jam Hasil</label>

                            <div class="col-md-3">
                                <input type="time" class="form-control pull-right" name="jam_hasil" placeholder="Tekan Simbol Jam" value="" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="sct" class="col-md-4 col-form-label text-md-right">Score CT</label>

                            <div class="col-md-6">
                            <input id="sct" type="number" step="0.01" class="form-control" name="sct" placeholder="Score CT" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="ct"class="col-md-4 col-form-label text-md-right">Hasil Pemeriksaan</label>
                            <div class="col-md-6">
                            <select name="ct" id="ct" class="form-control select2">
                            <option value="Positif">Positif</option>
                            <option value="Negatif">Negatif</option>
                            </select>
                            @error('ct')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                            </div>
                        </div>

                            <div class="form-group row">
                            <label for="petugas" class="col-md-4 col-form-label text-md-right">Petugas</label>

                            <div class="col-md-4">
                            <select name="petugas" id="petugas" class="form-control select2">
                            @foreach ($officers as $officer)
                                    <option value="{{$officer->nama}}">{{$officer->nama}}</option>
                            @endforeach
                                </select>
                            </div>
                            <button type="button" class="btn btn-success float-right mb-1" data-toggle="modal" data-target="#modalTambahBarang">Tambah</button>

                            </div>

                            <div class="form-group row">
                                <label for="ttl" class="col-md-4 col-form-label text-md-right">Tanggal Surat</label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control pull-right" id="dp" name="tgl_surat" value="" required>
                                </div>
                            </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Cetak Surat Keterangan') }}
                                </button>
                                <a href="{{ route('surat.index') }}" class="btn btn-danger">Kembali</a>

                            </div>
                        </div>
                    </form>
                </div>
                @include('surat.modal')
            </div>
        </div>
    </div>
</div>

@endsection
