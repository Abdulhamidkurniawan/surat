@extends('layouts.dashboard')
<script src="{{asset('bower_components/jquery/dist/jquery.min.js')}}"></script>
<script src="{{asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.js')}}"></script>
<script>
$.noConflict();
jQuery( document ).ready(function( $ ) {
  $( function() {
  $( "#datepicker" ).datepicker({
    format: 'dd MM yyyy'});
} );
});
// Code that uses other library's $ can follow here.
</script>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="{{asset('js/jquery-dropdown-datepicker.min.js')}}"></script>
<script>
$.noConflict();
jQuery( document ).ready(function( $ ) {
$("#date").dropdownDatepicker({
    defaultDateFormat: 'yyyy-mm-dd',
    displayFormat: 'dmy',
    minYear: 1940,
    allowFuture:false,
    submitFormat: 'yyyy-mm-dd'
});
});
</script>
@section('content')
<?php
setlocale(LC_ALL, 'IND');
echo strftime('%d %B %Y');?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">

    <div class="box">

            <!-- /.box-header -->
            <div class="box-body">

              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Nama</th>
                  {{-- <th>Kode</th> --}}
                  <th>Jenis</th>
                  <th>Harga</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($tests as $test)
                    <tr>
                      <td>{{$test->nama}}</td>
                      {{-- <td>{{$test->kode}}</td> --}}
                      <td>{{$test->jenis}}</td>
                      <td>{{$test->harga}}</td>
                    </tr>
                    @endforeach

                </tbody>

              </table>
                        <!-- <div class="form-group row">
                            <label for="tgl_periksa" class="col-md-1 col-form-label text-md-right">Tanggal </label>

                            <div class="col-md-2">
                                <input type="text" class="form-control pull-right" id="dari" name="tgl_periksa" value="" required>
                            </div>
                            <label for="tgl_periksa" class="col-md-1 col-form-label text-md-right">Hingga</label>

                            <div class="col-md-2">
                                <input type="text" class="form-control pull-right" id="ke" name="tgl_periksa" value="" required>
                            </div>
                        </div>
              <a href="{{ route('rm.rmreport') }}" class="btn btn-sm btn-primary">Export Data</a> -->
              <button type="button" class="btn btn-success float-right mb-1" data-toggle="modal" data-target="#modalTambahBarang">Tambah Data</button>
              <div class="modal fade" id="modalTambahBarang" tabindex="-1" aria-labelledby="modalTambahBarang" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                    <div class="modal-header">
                    <h5 class="modal-title">Tambah Data Pekerjaan</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                    </div>
                    <div class="modal-body">
                    <!--FORM TAMBAH BARANG-->
                    <form method="POST" action="{{ route('test.store') }}">
                    @csrf
                    <div class="form-group row">
                    <label for="tnama" class="col-md-4 col-form-label text-md-right">Tambah Tindakan</label>
                    <div class="col-md-4">
                    <input oninput="let p=this.selectionStart;this.value=this.value.toUpperCase();this.setSelectionRange(p, p);" id="tnama" type="text" class="form-control" name="tnama" value="{{ old('tnama') }}" autofocus placeholder="Nama Tindakan">
                    </div>
                    </div>
                    {{-- <div class="form-group row">
                        <label for="ttest" class="col-md-4 col-form-label text-md-right"></label>
                        <div class="col-md-4">
                        <input oninput="let p=this.selectionStart;this.value=this.value.toUpperCase();this.setSelectionRange(p, p);" id="tkode" type="text" class="form-control" name="tkode" value="{{ old('tkode') }}" autofocus placeholder="Kode">
                        </div>
                        </div> --}}
                        <div class="form-group row">
                            <label for="jenis" class="col-md-4 col-form-label text-md-right">Jenis</label>

                            <div class="col-md-4">
                            <select name="tjenis" id="tjenis" class="form-control select2">
                            @foreach ($jeniss as $jenis)
                                    <option value="{{$jenis->jenis_rm}}">{{$jenis->jenis_rm}}</option>
                            @endforeach
                                </select>
                            </div>
                            </div>
                            <div class="form-group row">
                                <label for="ttest" class="col-md-4 col-form-label text-md-right"></label>
                                <div class="col-md-4">
                                <input oninput="let p=this.selectionStart;this.value=this.value.toUpperCase();this.setSelectionRange(p, p);" id="tharga" type="text" class="form-control" name="tharga" value="{{ old('tharga') }}" autofocus placeholder="Harga">
                                </div>
                                <button type="submit" class="btn btn-primary">Tambah</button>
                                </div>
                    </form>
                    </div>
                    <div class="modal-body">
                    <!--FORM TAMBAH BARANG-->
                    <form method="POST" action="{{ route('test.destroy') }}">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                    <div class="form-group row">
                    <label for="ktest" class="col-md-4 col-form-label text-md-right"></label>
                    <div class="col-md-4">
                        <select name="ktest" id="ktest" class="form-control select2">
                            @foreach ($tests as $test)
                                    <option value="{{$test->id}}">{{$test->nama}}</option>
                            @endforeach
                                </select>
                    </div>
                    <button type="submit" class="btn btn-danger">Hapus</button>
                    </div>
                    {{-- <button type="button" onclick="myFunction()">Try it</button> --}}
                    </form>
                    </div>
                    </div>

                    <!--END FORM TAMBAH BARANG-->
                    </div>
                    </div>
                    </div>
                    </div>
            </div>
            <!-- /.box-body -->
          </div>
        <!-- right col -->
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 0.0.1
    </div>
    <strong>Copyright &copy; 2014-2020 <a href="#">BBDSG</a>
  </footer>

  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="{{asset('bower_components/jquery/dist/jquery.min.js')}}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{asset('bower_components/jquery-ui/jquery-ui.min.js')}}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="{{asset('bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- Morris.js charts -->
<script src="{{asset('bower_components/raphael/raphael.min.js')}}"></script>
<script src="{{asset('bower_components/morris.js/morris.min.js')}}"></script>
<!-- Sparkline -->
<script src="{{asset('bower_components/jquery-sparkline/dist/jquery.sparkline.min.js')}}"></script>
<!-- jvectormap -->
<script src="{{asset('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
<script src="{{asset('plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
<!-- jQuery Knob Chart -->
<script src="{{asset('bower_components/jquery-knob/dist/jquery.knob.min.js')}}"></script>
<!-- daterangepicker -->
<script src="{{asset('bower_components/moment/min/moment.min.js')}}"></script>
<script src="{{asset('bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
<!-- datepicker -->
<script src="{{asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.js')}}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
<!-- Slimscroll -->
<script src="{{asset('bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('bower_components/fastclick/lib/fastclick.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('dist/js/adminlte.min.js')}}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{asset('dist/js/pages/dashboard.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('dist/js/demo.js')}}"></script>
<!-- DataTables -->
<script src="{{asset('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script>
  $(function () {
    $('#example1').dataTable( {
      'aaSorting': []
  } );
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>

@endsection
