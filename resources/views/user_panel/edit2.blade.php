@extends('layouts.app') <!-- --> <!-- include-->

@section('content')
<div class="container">
    <div class="col-md-8 offset-md-2">
            <div class="card">
                    <div class="card-header">Edit User</div>
                        <div class="card-body">
                                <form class="" action="{{ route('user_panel.update', $user)}}" method="post">
                                        {{ csrf_field() }}
                                        {{ method_field('PATCH') }} <!-- membuat patch PostController bisa dibaca -->
                                        <div class="form-group row">
                                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                                            <div class="col-md-6">
                                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{$user->name}}" required autofocus>

                                                @if ($errors->has('name'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('name') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                            <div class="col-md-6">
                                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{$user->email}}" required>

                                                @if ($errors->has('email'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('email') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                                            <div class="col-md-6">
                                                <input id="password" type="text" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" value="{{$user->password}}" required>

                                                @if ($errors->has('password'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('password') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                                            <div class="col-md-6">
                                                <input id="password-confirm" type="text" class="form-control" name="password_confirmation" value="{{$user->password}}" required>
                                            </div>
                                        </div>
                                        <div class="form-group row{{ $errors->has('jabatan') ? ' has-error ' : '' }}">
                                            <label class="col-md-4 col-form-label text-md-right">{{ __('Jabatan') }}</label>

                                            <div class="col-md-6">
                                                <select name="jabatan" class="form-control">
                                                    @foreach ($jabatans as $jabatan)   <!-- $categories dari PostController -->
                                                    <option value="{{$jabatan->jabatan}}"
                                                    @if ($jabatan->jabatan === $user->jabatan)
                                                        selected
                                                    @endif
                                                    > {{$jabatan->jabatan}} </option>
                                                            @endforeach
                                                   </select>

                                            </div>
                                          </div>
                                        <div class="form-group row mb-0">
                                            <div class="col-md-6 offset-md-4">
                                                <button type="submit" class="btn btn-primary">
                                                    {{ __('Simpan') }}
                                                </button>
                                            </div>
                                        </div>
                                        </form>
                        </div>
                </div>
    </div>
</div>
@endsection
