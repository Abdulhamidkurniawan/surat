<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Auth::routes();

// Route::middleware('cors')->group(function() {
  // Route::post('/loginapps', function(){ return 'sukses'; });
// });
Route::post('/loginapps', 'Auth\LoginController@login')->name('loginapps');
Route::post('/logoutapps', 'Auth\LoginController@logout')->name('logoutapps');

Route::get('/', 'RedirectController@default')->name('default');
Route::get('/cari', 'RedirectController@cari')->name('cari');
Route::post('/cari', 'RedirectController@cek')->name('cek');
Route::get('/hasil', 'RedirectController@hasil')->name('hasil');

Route::middleware('auth')->group(function() {

Route::get('/dashboard', 'HomeController@dashboard')->name('dashboard');
Route::get('/optimize', 'HomeController@optimize')->name('optimize');

Route::middleware('admin')->group(function() {
    // Ruoute akun user
    Route::get('/user_panel', 'UserPanelController@index')->name('user_panel.index');
    Route::get('/user_panel/create', 'UserPanelController@create')->name('user_panel.create');
    Route::post('/user_panel/create', 'UserPanelController@store')->name('user_panel.store');
    // Route::get('/user_panel/{post}', 'UserPanelController@show')->name('user_panel.show');
    Route::get('/user_panel/{user}/edit', 'UserPanelController@edit')->name('user_panel.edit');
    Route::patch('/user_panel/{user}/edit', 'UserPanelController@update')->name('user_panel.update');
    Route::delete('/user_panel/{user}/delete', 'UserPanelController@destroy')->name('user_panel.destroy');

    Route::get('/surat', 'SuratController@index')->name('surat.index');
    Route::post('/surat/{pasien}/create_rapid', 'SuratController@storerapid')->name('surat.storerapid');
    Route::post('/surat/{pasien}/create_sa', 'SuratController@storesa')->name('surat.storesa');
    Route::post('/surat/{pasien}/create_si', 'SuratController@storesi')->name('surat.storesi');
    Route::post('/surat/{pasien}/create_sp', 'SuratController@storesp')->name('surat.storesp');
    Route::post('/surat/{pasien}/create_narkoba', 'SuratController@storenarkoba')->name('surat.storenarkoba');
    Route::post('/surat/{pasien}/create_narkoba3', 'SuratController@storenarkoba5')->name('surat.storenarkoba5');
    Route::get('/surat/{pasien}/rapid', 'SuratController@rapid')->name('surat.rapid');
    Route::get('/surat/{pasien}/sa', 'SuratController@sa')->name('surat.sa');
    Route::get('/surat/{pasien}/sp', 'SuratController@sp')->name('surat.sp');
    Route::get('/surat/{pasien}/si', 'SuratController@si')->name('surat.si');
    Route::get('/surat/{pasien}/sr_pcr', 'SuratController@sr_pcr')->name('surat.sr_pcr');
    Route::get('/surat/{pasien}/narkoba', 'SuratController@narkoba')->name('surat.narkoba');
    Route::get('/surat/{pasien}/narkoba5', 'SuratController@narkoba5')->name('surat.narkoba5');
    Route::delete('/surat/{surat}/delete', 'SuratController@destroy')->name('surat.destroy');
    Route::patch('/surat/{surat}/softdel', 'SuratController@softdel')->name('surat.softdel');
    Route::get('/rm', 'RmController@index')->name('rm.index');
    Route::get('/dihapus', 'RmController@dihapus')->name('rm.dihapus');
    Route::post('/rm/create', 'RmController@store')->name('rm.store');
    Route::delete('/rm/{rm}/delete', 'RmController@destroy')->name('rm.destroy');
    Route::get('/rm/rmreport', 'RmController@rmreport')->name('rm.rmreport');
    Route::post('/rm/exceltanggal', 'RmController@exceltanggal')->name('rm.exceltanggal');
    Route::post('/rm/excelbulan', 'RmController@excelbulan')->name('rm.excelbulan');
    Route::get('/bill', 'RmController@billindex')->name('bill.index');
    Route::get('/bill/{surat}/create', 'RmController@billcreate')->name('bill.create');
    Route::post('/bill/{surat}/cetak', 'RmController@billcetak')->name('bill.cetak');
    Route::post('/bill/cm', 'RmController@cm')->name('bill.cm');

    Route::get('/pasien', 'PasienController@index')->name('pasien.index');
    Route::get('/pasien/create', 'PasienController@create')->name('pasien.create');
    Route::post('/pasien/create', 'PasienController@store')->name('pasien.store');
    Route::get('/pasien/{pasien}/edit', 'PasienController@edit')->name('pasien.edit');
    Route::patch('/pasien/{pasien}/edit', 'PasienController@update')->name('pasien.update');
    Route::delete('/pasien/{pasien}/delete', 'PasienController@destroy')->name('pasien.destroy');

    Route::post('/pekerjaan/create', 'WorksController@store')->name('pekerjaan.store');
    Route::delete('/pekerjaan/delete', 'WorksController@destroy')->name('pekerjaan.destroy');
    Route::post('/pasien/excel', 'PasienController@excel')->name('pasien.excel');

    Route::post('/officer/create', 'OfficersController@store')->name('officer.store');
    Route::delete('/officer/delete', 'OfficersController@destroy')->name('officer.destroy');

    Route::get('/test', 'TestController@index')->name('test.index');
    Route::post('/test/create', 'TestController@store')->name('test.store');
    Route::delete('/test/delete', 'TestController@destroy')->name('test.destroy');
    });});
